
OPTIONS=$(getopt -o f:r:n:ch -n 'parse-options' -- "$@")

if [ $? -ne 0 ]; then
  echo "getopt error"
  exit 1
fi

comparison=0

echo "$OPTIONS"
eval set -- $OPTIONS
while true; do
  case "$1" in
    
    -f) FILE="$2" ; shift ;;
    -r) REF="$2" ; shift ;;
    -c) comparison=1;;
    -h) HELP=1 ;;
    --)        shift ; break ;;
    *)         echo "unknown option: $1" ; exit 1 ;;
  esac
  shift
done

if [ $# -ne 0 ]; then
  echo "unknown option(s): $@"
  exit 1
fi

echo "read file: $FILE"
echo "ref file: $REF"



refFile=$REF
correctedFile=$FILE

i=6
if  [ $comparison = 1 ]; then
	~/bin/blat $refFile $FILE output.psl > /dev/null 2>&1
	while read l1; do
		((nr = i))
		#~ echo $nr
		if [ $nr > 5 ]; then
			sizeC=$(awk -v var="$nr" 'NR == var {print $11}' output.psl)
			#~ echo $sizeC
			sizeRef=$(awk -v var="$nr" 'NR == var {print $15}' output.psl)
			nbMismatch=$(awk -v var="$nr" 'NR == var {print $2+$5+$7}' output.psl)
			if [ ! -z "$sizeRef" ]; then
				len=$(awk "BEGIN {printf \"%.2f\", ${sizeC}/${sizeRef}}")
				echo $((i-5)) $sizeC $sizeRef $len $nbMismatch
			fi
		fi
		((i++))
	done < "output.psl"
else
	while read l1 && read l2; do
		echo $l1$'\n'$l2 > queryFasta
		~/bin/blat $refFile queryFasta output.psl > /dev/null 2>&1
		sizeC=$(awk 'NR == 6 {print $11}' output.psl)
		sizeRef=$(awk 'NR == 6 {print $15}' output.psl)
		nbMismatch=$(awk 'NR == 6 {print $2+$5+$7}' output.psl)
		if [ ! -z "$sizeRef" ]; then
			len=$(awk "BEGIN {printf \"%.2f\", ${sizeC}/${sizeRef}}")
			echo $l1 $sizeRef $len $nbMismatch
		fi
		
	done < $correctedFile
fi



