#include <fstream>
#include <cstring>
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <unordered_set>
#include <unordered_map>

//~ #include "utils.hpp"
#include "backbone.hpp"

using namespace std;





vector<string> withoutAdj(unordered_map<string,vector<string>>& prev, unordered_map<string,vector<string>>& next, unordered_map<string,uint>& kmerCount, vector<string>& candidate){
	vector<string> nextCandidate,res,toSuppr;
	string kmer,son;
	if(candidate.size()==0){
		for(auto it(kmerCount.begin());it!=kmerCount.end();++it){
			kmer=(it->first);
			if(prev.count(kmer)==0 or prev[kmer].size()==0){
				//~ cout<<"noprevfound"<<endl;
 				toSuppr.push_back(kmer);
				res.push_back(kmer);
				for(uint j(0);j<next[kmer].size();++j){
					son=next[kmer][j];
					nextCandidate.push_back(son);
					auto newEnd = std::remove(prev[son].begin(), prev[son].end(), kmer);
					prev[son].erase(newEnd, prev[son].end());
				}
				next.erase(kmer);
				prev.erase(kmer);
			}
		}
		for(uint i(0);i<toSuppr.size();++i){
			kmerCount.erase(toSuppr[i]);
		}
	}else{
		for(uint i(0);i<candidate.size();++i){
			kmer=candidate[i];
			if(kmerCount.count(kmer)!=0){
				if(prev.count(kmer)==0 or prev[kmer].size()==0){
					//~ cout<<"noprevfound"<<endl;
					kmerCount.erase(kmer);
					res.push_back(kmer);
					for(uint j(0);j<next[kmer].size();++j){
						son=next[kmer][j];
						//~ if(kmerCount.count(son)!=0){
							nextCandidate.push_back(son);
						//~ }
						auto newEnd = std::remove(prev[son].begin(), prev[son].end(), kmer);
						prev[son].erase(newEnd, prev[son].end());
					}
					next.erase(kmer);
					prev.erase(kmer);
				}
			}else{
				//~ cout<<"wow"<<endl;
			}
		}
	}
	sort( nextCandidate.begin(), nextCandidate.end() );
	nextCandidate.erase( unique( nextCandidate.begin(), nextCandidate.end() ), nextCandidate.end() );
	candidate=nextCandidate;
	return res;
}



void eraseKmer(unordered_map<string,vector<string>>& prev, unordered_map<string,vector<string>>& next, unordered_map<string,uint>& kmerCount,string kmer){
	kmerCount.erase(kmer);
	string son;
	for(uint j(0);j<next[kmer].size();++j){
		son=next[kmer][j];
		auto newEnd = std::remove(prev[son].begin(), prev[son].end(), kmer);
		prev[son].erase(newEnd, prev[son].end());
	}
	next.erase(kmer);
	for(uint j(0);j<prev[kmer].size();++j){
		son=prev[kmer][j];
		auto newEnd = std::remove(next[son].begin(), next[son].end(), kmer);
		next[son].erase(newEnd, next[son].end());
	}
	prev.erase(kmer);
}



unordered_set<string> getFirstKmers(uint depth, unordered_map<string,uint>& kmerCount, vector<vector<string>>& sketchVector){
	unordered_set<string> res;
	for(uint i(0);i<sketchVector.size();++i){
		uint n(0);
		for(uint j(0);j<sketchVector[i].size();++j){
			if(kmerCount.count(sketchVector[i][j])==1){
				res.insert(sketchVector[i][j]);
				if(++n>=depth){
					break;
				}
			}
		}
	}
	//~ cout<<res.size()<<endl;
	return res;
}



unordered_set<string> getLastKmers(uint depth, unordered_map<string,uint>& kmerCount, vector<vector<string>>& sketchVector){
	unordered_set<string> res;
	for(uint i(0);i<sketchVector.size();++i){
		uint n(0);
		for(int j(sketchVector[i].size()-1);j>=0;--j){
			if(kmerCount.count(sketchVector[i][j])==1){
				res.insert(sketchVector[i][j]);
				if(++n>=depth){
					break;
				}
			}
		}
	}
	//~ cout<<res.size()<<endl;
	return res;
}


int THEFUNCTION(const vector<string>& readsVector, const uint k, const uint solid, vector<string>& kmersBackbonevector, vector<vector<uint>>& backbones, vector<uint>& distances){
	backbones.resize(readsVector.size());
	string query,read,header;
	kmersBackbonevector.clear();
	//KMER COUNTING
	unordered_map<string,uint> kmerCount,kmerCountLocal;
	unordered_set<string> duplicateKmer;
	for(uint i(0);i<readsVector.size();++i){
		read=readsVector[i];
		kmerCountLocal={};
		for(uint ii(0);ii+k<=read.size();++ii){
			kmerCountLocal[read.substr(ii,k)]++;
		}
		for(auto it(kmerCountLocal.begin()); it != kmerCountLocal.end(); ++it){
			if(it->second==1 and duplicateKmer.count(it->first)==0){
				kmerCount[it->first]++;
			}else{
				duplicateKmer.insert(it->first);
			}
		}
	}
	vector<string> toErase;
	for(auto it(kmerCount.begin()); it != kmerCount.end(); ++it){
		if(it->second<solid){
			toErase.push_back(it->first);
		}
	}
	for(uint i(0);i<toErase.size();++i){
		kmerCount.erase(toErase[i]);
	}

	//SKETCH CREATION
	string kmer;
	vector<vector<string>> sketchVector(readsVector.size());
	for(uint i(0);i<readsVector.size();++i){
		read=readsVector[i];
		for(uint ii(0);ii+k<=read.size();++ii){
			kmer=read.substr(ii,k);
			if(kmerCount.count(kmer)==1){
				sketchVector[i].push_back(kmer);
			}
		}
	}

	vector<string> sketch;
	unordered_map<string,vector<string>> next,prev;
	//ADJ CREATION
	for(uint i(0);i<sketchVector.size();++i){
		sketch=sketchVector[i];
		for(uint ii(0);ii+1<sketch.size();++ii){
			next[sketch[ii]].push_back(sketch[ii+1]);
			prev[sketch[ii+1]].push_back(sketch[ii]);
		}
	}
	vector<string> resultBegin, resultEnd, noPrev, noNext, result,candidateNext,candidatePrev;
	unordered_set<string> kmersBegin,kmersEnd;
	//MAIN LOOP
	while(not kmerCount.empty()){
		uint count(kmerCount.size());
		noPrev=withoutAdj(prev, next,kmerCount,candidatePrev);
		noNext=withoutAdj(next, prev,kmerCount,candidateNext);
		if(noPrev.size()==1){
			resultBegin.push_back(noPrev[0]);
		}
		if(noNext.size()==1){
			resultEnd.push_back(noNext[0]);
		}
		//IF PARADOX
		if(kmerCount.size()==count){
			bool found(false);
			uint depth(1);
			while(not found){
				if(kmerCount.size()<2*depth){
					kmerCount={};
					break;
				}
				kmersBegin=getFirstKmers(depth,kmerCount,sketchVector);
				kmersEnd=getLastKmers(depth,kmerCount,sketchVector);
				for(auto it(kmersBegin.begin()); it != kmersBegin.end() and not found; ++it){
					if(kmersEnd.count(*it)==1){
						eraseKmer(prev,next,kmerCount,*it);
						found=true;
						//~ break;
					}
				}
				candidatePrev=candidateNext={};
				depth*=2;
			}
		}
	}
	reverse(resultEnd.begin(), resultEnd.end());
	resultBegin.insert(resultBegin.end(), resultEnd.begin(), resultEnd.end());
	unordered_map<string, uint> kmersBackbone;
	for(uint i(0); i < resultBegin.size(); ++i){
		kmersBackbone.insert({resultBegin[i], i});
	}
	for(uint i(0); i < readsVector.size(); ++i){
		read = readsVector[i];
		int last(-1);
		for(uint ii(0); ii + k <= read.size(); ++ii){
			kmer = read.substr(ii,k);
			if (kmersBackbone.count(kmer)){
				uint indice(kmersBackbone[kmer]);
				if((int)indice<last){
					kmersBackbone.erase(kmer);
					cout<<kmer<<endl;
					//~ cin.get();
				}else{
					last=indice;
				}
			}
		}
	}
	//~ unordered_map<string, uint> kmersBackbone2;
	//~ uint count(0);
	//~ for(uint i(0); i < resultBegin.size(); ++i){
		//~ if(kmersBackbone.count(resultBegin[i])==0){
			//~ kmersBackbone2.insert({resultBegin[i], count++});
		//~ }
	//~ }
	

	uint countNew(0);
	//PRINT BACKBONE
	for(uint i(0); i < resultBegin.size(); ++i){
		if(kmersBackbone.count(resultBegin[i])==1){
			kmersBackbone[resultBegin[i]]=countNew++;
			kmersBackbonevector.push_back(resultBegin[i]);
		}
	}
	vector<uint> distancesWeight;
	distances.resize(kmersBackbone.size());
	distancesWeight.resize(kmersBackbone.size());
	//PRINT for each reads the backbone
	for(uint i(0); i < readsVector.size(); ++i){
		int pred(-2);
		uint predPosition(0);
		int actu;
		read = readsVector[i];
		for(uint ii(0); ii + k <= read.size(); ++ii){
			kmer = read.substr(ii,k);
			if (kmersBackbone.count(kmer)){
				actu=kmersBackbone[kmer];
				backbones[i].push_back( actu );
				if(pred+1==actu){
					distances[actu]+=ii-predPosition;
					distancesWeight[actu]++;
				}
				pred=actu;
				predPosition=ii;
			}
		}
	}
	for(uint i(1);i<distances.size()-1;++i){
		if(distancesWeight[i]!=0){	
			distances[i]/=distancesWeight[i];
		}else{
			//~ cin.get();
		}
	}
	return 0;
}
