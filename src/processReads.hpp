#include <vector>
#include <string>
#include <iostream>
#include <unordered_map>
#include <unordered_set>

#include "utils.hpp"
#include "backbone.hpp"

#ifndef PROCESS
#define PROCESS




using namespace std;

typedef unsigned int uint;



void readDataset(ifstream& readFile, vector<string>& readSet);
unordered_map<string, vector<pair<uint, uint>>>	 compactBackbone(vector<string>& backbone, uint sizeK, uint minOverlap );

//~ void getWindowsInQuery(uint indexQuery, vector<string>& vecReads, vector<string>& backbone, vector<vector<uint>>& SSNperRead, uint k, uint w, vector<pair<uint, uint>>& listSSKWindows, vector<pair<int, int>>& listPairsSSKWindows);

void getWindowsInEachRead(vector<string>& vecReads, vector<string>& backbone, vector<vector<uint>>& SSNperRead, vector<pair<int, int>>& listPairsSSKWindows, vector<vector<pair<int, int>>>& listWindows);


void chopReadsInWindows(vector<pair<uint, uint>>& listSSKWindows, uint k, vector<string>& subseqVec, vector<vector<pair<int, int>>>& listWindows, uint pair, uint indexQuery, vector<string>& vecReads, vector<string>& backbone, vector<vector<uint>>& SSNperRead);


//~ void getWindowsOnReads(vector<string>& vecReads, vector<string>& backbone, vector<vector<uint>>& SSNperRead, uint k, uint w, uint solidity);





void correctQuery(vector<string>& vecCons, vector<string>& vecReads, vector<string>& newVecReads, vector<vector<uint>>& SSKmersPerRead, uint indexQuery, uint k, vector<string>& backbone);


bool readBackboneFile(ifstream& file, vector<string>& backbone, vector<vector<uint>>& SSKmersPerRead, vector<vector<uint>>& SSKmersPosiPerRead);
//~ bool getReadsAndBackbone(ifstream& file, vector<string>& readSet, uint k, uint solid, vector<string>& backbone, vector<vector<uint>>& SSKmersPerRead, vector<vector<uint>>& SSKmersPosiPerRead);

void getWindowsOnReads(vector<string>& vecReads, vector<string>& backbone, vector<uint>& distancesSSN, uint k, uint w, vector<pair<int, int>>& listPairsSSKWindows);

#endif
