#include <fstream>
#include <cstring>
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <unordered_set>
#include <unordered_map>
#include <sstream>


#ifndef UT
#define UT
using namespace std;



char revCompChar(char c);


string revComp(const string& s);



string getCanonical(const string& str);

vector<string> split(const string &s, char delim);

#endif
