#include <vector>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <unordered_set>

#include "consensus.hpp"
#include "test.hpp"


using namespace std;

//~ uint THRESHOLD_SOLIDITY(2);
//~ uint MIN_OVERLAP(5);


int main(int argc, char ** argv){
	if (argc > 3){
		string readFileName(argv[1]);
		uint k(uint(stoi(argv[2])));
		uint solidity(uint(stoi(argv[3])));
		ifstream readFile(readFileName);
		correctData(readFile, k, solidity);
		//~ bool passed;
		//~ passed = test_getWindowsInQuery();
		//~ if (passed){
			//~ cout << "test windows... passed" << endl;
		//~ } else {
			//~ cout << "test windows... not passed" << endl;
		//~ }
	} else {
		cout << "Usage: ./consensus <readFile.fa> <> <k> <solidity_threshold>" << endl;
	}
	return 0;
}
