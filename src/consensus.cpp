
#include <vector>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <unordered_set>

#include "consensus.hpp"




using namespace std;

uint THRESHOLD_SOLIDITY(2);
uint MIN_OVERLAP(5);

// INPUT of the consensus : a read file completed (fasta, we assume that the first read is the query read) by a
// a list of the synchronized solid k-mers that is contained in each read (vector of vectors (each index of the vector of vector corresponds to a read, in each vector is the list of k-mers))
// AND a vector of k-mers in that represents the ordered backbone



bool processReads(ifstream& readFile, uint k, uint solidity, vector<string>& backbone, vector<vector<uint>>& SSNperRead, vector<uint>& distancesSSN, vector<string>& vecReads){

	cout << "**********" << endl;
	uint w(60);
	//~ uint k(3), w(5);
	cout << k << " " << w << endl;
	readDataset(readFile, vecReads);
	cout << "reads " << vecReads.size() << endl;
	//~ vector<string> vecReads{"ACGTAGTGTTAGCGGATGTTACA", "AACGTAGTTTAGCGGATGTTAC", "CGTAGTGTTAGCAGGATGTTTACA"};
	//~ vector<string> backbone;
	//~ vector<vector<uint>> SSNperRead;
	//~ vector<vector<uint>> distancesSSN;
	//~ THEFUNCTION(vecReads, k, solidity, backbone, SSNperRead, SSNPosiperRead);
	//~ THEFUNCTION(vecReads, k, solidity, backbone, SSNperRead, distancesSSN);
	THEFUNCTION(vecReads, k, solidity, backbone, SSNperRead, distancesSSN);

	cout <<"bone done " << backbone.size() << endl;
	//~ for (uint i :distancesSSN){
		//~ cout << i << endl;
	//~ }
	vector<pair<int, int>> listPairsSSKWindows;
	getWindowsOnReads(vecReads, backbone, distancesSSN, k,  w, listPairsSSKWindows);
	vector<vector<pair<int, int>>> listWindows;
	getWindowsInEachRead(vecReads, backbone, SSNperRead, listPairsSSKWindows, listWindows);

	return backbone.size() > 0;
	//~ getWindowsOnReads(vecReads,  backbone, SSNperRead,  k,  w, solidity);
}



void toGFA(Graph& graph, uint k, uint solidityT, ofstream& file){
	file << "H\tVN:Z:1.0" << endl;
	vector<Node> neigh;
	for (auto n(graph.listNodes2.begin()); n != graph.listNodes2.end(); ++n){
		if (graph.vecNodesGraph[n->second].solidity >= solidityT){
			file << "S\t" << graph.vecNodesGraph[n->second].index << "\t" << n->first << endl;
			neigh = graph.vecNodesGraph[n->second].findOutNeighbour(graph.listNodes2, graph.vecNodesGraph);
			for (Node v : neigh){
				if (not v.isRepeat){
					if (v.solidity >= solidityT){
						file << "L\t" << graph.vecNodesGraph[n->second].index << "\t+\t" << v.index << "\t+\t" << k-1<< "M"<< endl;
					}
				} else {
					file << "L\t" << graph.vecNodesGraph[n->second].index << "\t+\t" << v.index << "\t+\t" << k-1<< "M"<< endl;
				}
			}
		}
	}
}

void toGfaWalk(Graph& graph, uint k, uint solidityT, vector<vector<Node>>& walk, ofstream& file){
	file << "H\tVN:Z:1.0" << endl;
	ofstream of("backbone.csv");
	of << "Name,isSSNode" << endl;
	vector<Node> neigh;
	uint j;
	for (uint i(0); i < walk.size(); ++i){
		if (not walk[i].empty()){
			if (i == 0){
				j = walk[i].size() - 1;
				while(j >= 1){
					file << "S\t" << walk[i][j].index << "\t" << walk[i][j].kmer << endl;
					if (walk[i][j].isSSNode){
						of << walk[i][j].index << "," << "S" <<  walk[i][j].SSNodes[0].first + 1  << endl;
					}
					if (walk[i][j].isRepeat){
						of << walk[i][j].index << "," << "R" << endl;
					}
					file << "L\t" << walk[i][j].index << "\t+\t" << walk[i][j-1].index << "\t+\t" << walk[i][j].overlapToNext << "M"<< endl;
					--j;
				}
				file << "S\t" << walk[i][j].index << "\t" << walk[i][j].kmer << endl;
				if (walk[i][j].isSSNode){
					of << walk[i][j].index << "," << "S" <<  walk[i][j].SSNodes[0].first + 1  << endl;
				}
				if (walk[i][j].isRepeat){
						of << walk[i][j].index << "," << "R" << endl;
					}
			} else {
				j = 0;
				while(j < walk[i].size() - 1){
					file << "S\t" << walk[i][j].index << "\t" << walk[i][j].kmer << endl;
					if (walk[i][j].isSSNode){
						of << walk[i][j].index << "," << "S" <<  walk[i][j].SSNodes[0].first + 1  << endl;
					}
					if (walk[i][j].isRepeat){
						of << walk[i][j].index << "," << "R" << endl;
					}
					file << "L\t" << walk[i][j].index << "\t+\t" << walk[i][j+1].index << "\t+\t" << walk[i][j].overlapToNext << "M"<< endl;
					++j;
				}
				file << "S\t" << walk[i][j].index << "\t" << walk[i][j].kmer << endl;
				if (walk[i][j].isSSNode){
					of << walk[i][j].index << "," << "S" <<   walk[i][j].SSNodes[0].first + 1 << endl;
				}
				if (walk[i][j].isRepeat){
						of << walk[i][j].index << "," << "R" << endl;
				}
			}
		}
	}
}



// consensus pre-treatment function: synchronisation of reads by backbone production //






// ----- Class Node ----- //

Node::Node(){
	index = 0;
	kmer = "";
}

Node::Node(const Node& node){
	index = node.index;
	kmer = node.kmer;
	isSSNode = node.isSSNode;
	isRepeat = node.isRepeat;
	SSNodes = node.SSNodes;
	solidity = node.solidity;
	seen = node.seen;
	overlapToNext = node.overlapToNext;
}



// constructor for regular and repeated nodes
Node::Node(uint indexI, const string& kmerI, uint seenI, int indexPrevSSN, int indexNextSSN, bool repeated){
	index = indexI;
	kmer = kmerI;
	seen = seenI;
	isSSNode = false;
	isRepeat = repeated;
	SSNodes = {{indexPrevSSN, indexNextSSN}};
	solidity = 1;
	overlapToNext = kmerI.size() - 1;
}




// constructor for backbone nodes (synchronised solid nodes)
Node::Node(uint indexI, const string& kmerI, int indexThisSSN){
	index = indexI;
	kmer = kmerI;
	isSSNode = true;
	isRepeat = false;
	SSNodes = {{indexThisSSN - 1, indexThisSSN + 1}};
	solidity = 1;
	seen = 0;
	overlapToNext = kmerI.size() - 1;
}


void Node::update(int indexPrevSSN, int indexNextSSN, bool repeat){ 
	isRepeat = repeat;
	if (isRepeat){  // REPEAT NODE
		SSNodes.push_back({indexPrevSSN, indexNextSSN});
		++solidity;
	} else if (not isSSNode) { // REGULAR NODE
		++solidity;
		// CASE 1 SSP < new SSP AND new SSP < SSN = replace SSP by new SSP
		if (SSNodes[0].first < indexPrevSSN and indexPrevSSN < SSNodes[0].second){
			SSNodes[0].first = indexPrevSSN;  // found a closer next synchronized solid  node = better precision
		}
		// CASE 2 SSN > new SSN AND new SSN > SSP = replace SSN by new SSN
		if (SSNodes[0].second > indexNextSSN and indexNextSSN > SSNodes[0].first){
			SSNodes[0].second = indexNextSSN;  // found a closer previous synchronized solid  node = better precision
		}
		// CASE 3 where we change a regular into a repeat
		// if new SSN <= SSP or new SSP >= SSN 
		if (indexPrevSSN >= SSNodes[0].second or indexNextSSN <= SSNodes[0].first){
			++solidity;
			isRepeat = true;
			SSNodes.push_back({indexPrevSSN, indexNextSSN});
		}
	}
}

void Node::updateRepeat(){
	std::sort(SSNodes.begin(), SSNodes.end()); // is supposed sort first by 1st element, then by 2nd
	vector <pair<int, int>> vCopy;
	uint i(1);
	bool modified(false);
	if (SSNodes.size() > 1){
		if (SSNodes[0].first != SSNodes[1].first){
			vCopy.push_back(SSNodes[0]);
			modified = true;
		}
		while (i < SSNodes.size()){
			if (SSNodes[i].first != SSNodes[i-1].first){
				vCopy.push_back(SSNodes[i]);
				modified = true;
			}
			++i;
		}
		if (modified){
			SSNodes = vCopy;
		}
	}
	
	vector <pair<int, int>> vCopy2;
	i = 0;
	modified = false;
	if (SSNodes.size() > 1){
		while (i < SSNodes.size() - 1){
			if (not((SSNodes[i].first <= SSNodes[i+1].first and SSNodes[i+1].second <= SSNodes[i].second))){
				vCopy2.push_back(SSNodes[i]);
				modified = true;
			}
			++i;
		}
		if (not((SSNodes[i-1].first <= SSNodes[i].first and SSNodes[i].second <= SSNodes[i-1].second))){
			vCopy2.push_back(SSNodes[i]);
			modified = true;
		}
		if (modified){
			SSNodes = vCopy2;
		}
	}
	//~ while (i < SSNodes.size() - 1){
		//~ modified = true;
		//~ if (SSNodes[i].first <= SSNodes[i+1].first){
			//~ if (vCopy.empty()){
				//~ vCopy.push_back(SSNodes[i]);
				//~ last = SSNodes[i].first;
			//~ } else if (vCopy.back().first != last){
				//~ vCopy.push_back(SSNodes[i]);
				//~ last = SSNodes[i].first;
			//~ }
		//~ } else {
			//~ vCopy.push_back(SSNodes[i]);
			//~ last = SSNodes[i].first;
		//~ }
		//~ ++i;
	//~ }
	
	solidity /= SSNodes.size();
	if (solidity == 0){
		solidity = 1;
	}
}




void Node::changeSSN(unordered_map<string, uint>& listNodes2, uint k, vector<Node>& vecNodesGraph){
	// todo: should a node below the solidity threshold give inheritance ? I think yes
	vector<Node> inNeighbourVect = findInNeighbour(listNodes2, k, vecNodesGraph);
	bool pass(false);
	for (uint n(0); n < inNeighbourVect.size(); ++n){  // if one precedent node is a SSK, stop the inheritance
		if (inNeighbourVect[n].isSSNode){
			pass = true;
		}
	}
	if (not pass){
		for (uint n(0); n < inNeighbourVect.size(); ++n){
			Node current(inNeighbourVect[n]);
			if (current.index != index){  // a node should not heritate from itself (particular case of branching)
				if (isSSNode){   // heritate from a SSN
					if (not current.isRepeat){
						if (current.SSNodes[0].second > SSNodes[0].second){  // SSNodes[0].first + 1 is the rank of the SSN (same as SSNodes[0].second - 1)
							current.SSNodes[0].second = SSNodes[0].second;  // found a closer next synchronized solid  node = better precision
							vecNodesGraph[listNodes2.at(current.kmer)] = current;  // change it in the list
							current.changeSSN(listNodes2, k, vecNodesGraph);  // for a regular node we continue to propagate the new SSN
						}
					} else {
						for (uint i(0); i < current.SSNodes.size(); ++i){
							if ((current.SSNodes[i].first <  SSNodes[0].first and SSNodes[0].second <= current.SSNodes[i].second) or (current.SSNodes[i].first <=  SSNodes[0].first and SSNodes[0].second < current.SSNodes[i].second)){ // found jointly closer next SSN and previous SSN
								pair<int,int> p(SSNodes[0].first, SSNodes[0].second);
								current.SSNodes.push_back(p);
								vecNodesGraph[listNodes2.at(current.kmer)] = current;
								current.changeSSN(listNodes2, k, vecNodesGraph);  // repeat node can propagate a pair of SSN to regular nodes
							}
						}
					}
				} else {  // heritate from a repeat or a regular node
					if (not current.isRepeat and not current.isSSNode){  
						if (not isRepeat){ // regular node heritates from regular node
							if (current.SSNodes[0].second > SSNodes[0].second and SSNodes[0].second > current.SSNodes[0].first){
								current.SSNodes[0].second = SSNodes[0].second;  // found a closer next synchronized solid  node = better precision
								vecNodesGraph[listNodes2.at(current.kmer)] = current;  // change it in the list
								current.changeSSN(listNodes2, k, vecNodesGraph);  // for a regular node we continue to propagate the new SSN
							}
						} else {  // regular node heritates from repeat
							for (uint i(0); i < current.SSNodes.size(); ++i){
								if (current.SSNodes[i].first <=  SSNodes[0].first and SSNodes[0].second <= current.SSNodes[0].second){
									current.SSNodes[i].second = SSNodes[0].second;
									vecNodesGraph[listNodes2.at(current.kmer)] = current;  // change it in the list
									current.changeSSN(listNodes2, k, vecNodesGraph);  // for a regular node we continue to propagate the new SSN
								}
							}
						}
					} else if (current.isRepeat and not isRepeat){  // we make only regular nodes inherit from repeat nodes => repeat nodes do not inherit from repeat nodes, and they do not propagate
						for (uint i(0); i < current.SSNodes.size(); ++i){
							if ((current.SSNodes[i].first <  SSNodes[0].first and SSNodes[0].second <= current.SSNodes[i].second) or (current.SSNodes[i].first <=  SSNodes[0].first and SSNodes[0].second < current.SSNodes[i].second)){ // found jointly closer next SSN and previous SSN
								pair<int,int> p(SSNodes[0].first, SSNodes[0].second);
								current.SSNodes.push_back(p);
								vecNodesGraph[listNodes2.at(current.kmer)] = current;
								current.changeSSN(listNodes2, k, vecNodesGraph);  // repeat node can propagate a pair of SSN to regular nodes
							}
						}
					}
				}
			}
		}
	}
}
void Node::changeSSNOut(unordered_map<string, uint>& listNodes2, uint k, vector<Node>& vecNodesGraph){
	// todo: should a node below the solidity threshold give inheritance ? I think yes
	vector<Node> outNeighbourVect;
	outNeighbourVect = findOutNeighbour(listNodes2, vecNodesGraph);
	bool pass(false);
	for (uint n(0); n < outNeighbourVect.size(); ++n){  // if one precedent node is a SSK, stop the inheritance
		if (outNeighbourVect[n].isSSNode){
			pass = true;
		}
	}
	if (not pass){
		for (uint n(0); n < outNeighbourVect.size(); ++n){
			Node current(outNeighbourVect[n]);
			if (current.index != index){  // a node should not heritate from itself (particular case of branching)
				if (isSSNode){   // heritate from a SSN
					if (not current.isRepeat){
						if (current.SSNodes[0].first < SSNodes[0].first){  // SSNodes[0].first + 1 is the rank of the SSN (same as SSNodes[0].second - 1)
							current.SSNodes[0].first = SSNodes[0].first;  // found a closer next synchronized solid  node = better precision
							vecNodesGraph[listNodes2.at(current.kmer)] = current;  // change it in the list
							current.changeSSNOut(listNodes2, k, vecNodesGraph);  // for a regular node we continue to propagate the new SSN
						}
					} else {
						for (uint i(0); i < current.SSNodes.size(); ++i){
							if ((current.SSNodes[i].first <  SSNodes[0].first and SSNodes[0].second <= current.SSNodes[i].second) or (current.SSNodes[i].first <=  SSNodes[0].first and SSNodes[0].second < current.SSNodes[i].second)){ // found jointly closer next SSN and previous SSNfound jointly closer next SSN and previous SSN
								pair<int,int> p(SSNodes[0].first, SSNodes[0].second);
								current.SSNodes.push_back(p);
								vecNodesGraph[listNodes2.at(current.kmer)] = current;
								current.changeSSNOut(listNodes2, k, vecNodesGraph);  // repeat node can propagate a pair of SSN to regular nodes
							}
						}
					}
				} else {  // heritate from a repeat or a regular node
					if (not current.isRepeat and not current.isSSNode){  
						if (not isRepeat){ // regular node heritates from regular node
							if (current.SSNodes[0].first < SSNodes[0].first and SSNodes[0].first < current.SSNodes[0].second){
								current.SSNodes[0].first = SSNodes[0].first;  // found a closer previous synchronized solid  node = better precision
								vecNodesGraph[listNodes2.at(current.kmer)] = current;  // change it in the list
								current.changeSSNOut(listNodes2, k, vecNodesGraph);  // for a regular node we continue to propagate the new SSN
							}
						} else {  // regular node heritates from repeat
							for (uint i(0); i < current.SSNodes.size(); ++i){
								if (current.SSNodes[i].first <=  SSNodes[0].first and SSNodes[0].second <= current.SSNodes[0].second){
									//~ current.SSNodes[i].second = SSNodes[0].second;
									current.SSNodes[i].first = SSNodes[0].first;
									vecNodesGraph[listNodes2.at(current.kmer)] = current;  // change it in the list
									current.changeSSNOut(listNodes2, k, vecNodesGraph);  // for a regular node we continue to propagate the new SSN
								}
							}
						}
					} else if (current.isRepeat and not isRepeat){  // we make only regular nodes inherit from repeat nodes => repeat nodes do not inherit from repeat nodes, and they do not propagate
						for (uint i(0); i < current.SSNodes.size(); ++i){
							if ((current.SSNodes[i].first <  SSNodes[0].first and SSNodes[0].second <= current.SSNodes[i].second) or (current.SSNodes[i].first <=  SSNodes[0].first and SSNodes[0].second < current.SSNodes[i].second)){ // found jointly closer next SSN and previous SSN
								pair<int,int> p(SSNodes[0].first, SSNodes[0].second);
								current.SSNodes.push_back(p);
								vecNodesGraph[listNodes2.at(current.kmer)] = current;
								current.changeSSNOut(listNodes2, k, vecNodesGraph);  // repeat node can propagate a pair of SSN to regular nodes
							}
						}
					}
				}
			}
		}
	}
}




vector<Node> Node::findOutNeighbour(const unordered_map<string, uint>& listNodes2, vector<Node>& vecNodesGraph){
	vector<Node> neighbours;
	string putativeNeighbour;
	for (char n : {'A','C','T','G'}){
		putativeNeighbour = kmer.substr(1) + n;
		if (listNodes2.count(putativeNeighbour)){
			neighbours.push_back(vecNodesGraph[listNodes2.at(putativeNeighbour)]);
		}
	}
	return neighbours;
}


vector<string> increaseByPermut(vector<string>& before){
	vector<string> lol;
	vector<char> nt = {'A', 'C', 'G', 'T'};
	for (uint i(1); i < 5; ++i){
		rotate(nt.begin(), nt.begin() + 1, nt.end());
		for (string n: before){
			n.push_back(nt[0]);
			lol.push_back(n);
		}
	}
	return lol;
}


vector<Node> Node::findOutKNeighbour(const unordered_map<string, uint>& listNodes2, uint overlap, vector<Node>& vecNodesGraph){
	string common(kmer.substr(kmer.size() - overlap));
	vector<Node> neighbours;
	string putativeNeighbour;
	vector<string> nt2 = {"A", "C", "G", "T"};
	vector<string> lol(nt2);
	for (uint i(0); i < kmer.size() - overlap - 1; ++i){
		lol = increaseByPermut(lol);
	}
	
	for (string n: lol){
		putativeNeighbour = common + n;
		if (listNodes2.count(putativeNeighbour)){
			neighbours.push_back(vecNodesGraph[listNodes2.at(putativeNeighbour)]);
		}
	}

	return neighbours;
}




vector<Node> Node::findInNeighbour(const unordered_map<string, uint>& listNodes2, uint k, vector<Node>& vecNodesGraph){
	vector<Node> neighbours;
	string putativeNeighbour;
	for (char n : {'A','C','T','G'}){
		putativeNeighbour = n + kmer.substr(0, k - 1);
		if (listNodes2.count(putativeNeighbour)){
			neighbours.push_back(vecNodesGraph[listNodes2.at(putativeNeighbour)]);
		}
	}
	return neighbours;
}





// ----- Class Graph ----- //


Graph::Graph(const vector<string>& vecReads, const vector<vector<uint>>& SSKmersPerRead, const vector<string>& backboneI, uint k){
	sizeK = k;
	unordered_set<string> kmersInRead;
	string kmer;
	bool repeat(false), SSkmer(false);
	uint indexRead(0),  index(0), indexSS;
	int indexPrevSSN, indexNextSSN;
	backbone = backboneI;
	for (string read : vecReads){
		if (not SSKmersPerRead[indexRead].empty()){ // test if read is not without backbone kmer
			indexSS = 0;
			indexPrevSSN = -1, indexNextSSN = SSKmersPerRead[indexRead][indexSS];
			for (uint posi(0); posi < read.size() - k + 1; ++posi){
				SSkmer = false;
				kmer = read.substr(posi, sizeK);
				// first check if the kmer is repeated within the read
				if (kmersInRead.count(kmer)){ // repeat
					repeat = true;
				} else {
					repeat = false;
					kmersInRead.insert({kmer});
				}
				// check if the kmer is a SS kmer
				if (SSKmersPerRead[indexRead][indexSS] < backbone.size()){
					if (kmer == backbone[SSKmersPerRead[indexRead][indexSS]] and indexSS < SSKmersPerRead[indexRead].size() - 1){
						SSkmer = true;
						indexPrevSSN = SSKmersPerRead[indexRead][indexSS];
						indexNextSSN = SSKmersPerRead[indexRead][indexSS + 1];
						++indexSS;
					} else if (kmer == backbone[SSKmersPerRead[indexRead][indexSS]] and indexSS >= SSKmersPerRead[indexRead].size() - 1 and indexSS < SSKmersPerRead[indexRead].size()) { // after the last SSkmer
						SSkmer = true;
						indexNextSSN = backbone.size(), indexPrevSSN = SSKmersPerRead[indexRead][indexSS];
					}
				} else { // we went over the last SS kmer
					indexNextSSN = backbone.size(), indexPrevSSN = SSKmersPerRead[indexRead][indexSS];
				}
				// then check if the kmer already exists in the graph
				if (listNodes2.count(kmer)){  // update the node
					vecNodesGraph[listNodes2.at(kmer)].update(indexPrevSSN, indexNextSSN, repeat);
				} else {  // create the node
					if (SSkmer){  // if the node is a SS node
						Node node(index, kmer, indexPrevSSN);
						vecNodesGraph.push_back(node);
						listNodes2.insert({kmer, vecNodesGraph.size() - 1});
					} else { // regular or repeat node
						Node node(index, kmer, 0, indexPrevSSN, indexNextSSN, repeat);
						vecNodesGraph.push_back(node);
						listNodes2.insert({kmer, vecNodesGraph.size() - 1});
					}
					++index;
				}
			}
			kmersInRead.clear();
		}
		++indexRead;
	}
}




void Graph::updateNodes(){
	vector<Node> neighbours;
	// 1 update Repeats
	for (auto itNode(listNodes2.begin()); itNode!= listNodes2.end(); ++itNode){
		if (vecNodesGraph[itNode->second].isRepeat){
			vecNodesGraph[itNode->second].updateRepeat();
		}
	}
	// 2 change nodes according to neighbouring
	for (uint bb(backbone.size()); bb > 0; --bb){
		
		vecNodesGraph[listNodes2.at(backbone[bb - 1])].changeSSN(listNodes2, sizeK, vecNodesGraph);
	}
	for (uint bb(0); bb < backbone.size(); ++bb){
		vecNodesGraph[listNodes2.at(backbone[bb])].changeSSNOut(listNodes2, sizeK, vecNodesGraph);
	}
	for (auto iterNode(listNodes2.begin()); iterNode != listNodes2.end(); ++iterNode){
		vecNodesGraph[iterNode->second].changeSSN(listNodes2, sizeK, vecNodesGraph);
	}
	// 3 update Repeats after change
	for (auto itNode(listNodes2.begin()); itNode!= listNodes2.end(); ++itNode){
		if (vecNodesGraph[itNode->second].isRepeat){
			vecNodesGraph[itNode->second].updateRepeat();
		}
	}
}




bool Graph::getBeforeFirst(vector<Node>& walk, uint solidityThreshold){
	bool pushed(false), found(false);
	vector<Node> neighbours(walk.back().findInNeighbour(listNodes2, sizeK, vecNodesGraph));
	sort(neighbours.begin(), neighbours.end(), compareNodesBySolid);
	for (uint n(0); n < neighbours.size(); ++n){
		Node current(neighbours[n]);
		vector<Node> neighbours2(current.findInNeighbour(listNodes2, sizeK, vecNodesGraph));
		if (neighbours2.empty()){ // beginning of the sequence
			if (not current.isRepeat){
				if (current.SSNodes[0].first == -1 and current.solidity >= solidityThreshold){
					walk.push_back(current);
					found = true;
					break;
				} else {
					found = false;
				}
			} else {
				for (uint i(0); i < current.SSNodes.size(); ++i){
					if (current.SSNodes[i].first == -1){
						walk.push_back(current);
						found = true;
						break;
					} else {
						found = false;
					}
				}
			}
		}
		if (not found and not neighbours2.empty()) {
			if (not current.isRepeat){
				if (current.solidity >= solidityThreshold){
					if (current.SSNodes[0].first == -1 and current.seen < 3){
						walk.push_back(current);
						++vecNodesGraph[listNodes2.at(current.kmer)].seen;
						pushed = true;
						break;
					}
				}
			} else {
				for (uint i(0); i < current.SSNodes.size(); ++i){
					if (current.SSNodes[i].first == -1 and current.seen < current.SSNodes.size() + 1){
						walk.push_back(current);
						++vecNodesGraph[listNodes2.at(current.kmer)].seen;
						pushed = true;
						break;
					}
				}
			}
		}
	}
	return pushed;
}


bool Graph::getAfterLast(vector<Node>& walk, uint solidityThreshold){
	bool pushed(false), found(false);
	vector<Node> neighbours(walk.back().findOutNeighbour(listNodes2, vecNodesGraph));
	sort(neighbours.begin(), neighbours.end(), compareNodesBySolid);
	for (uint n(0); n < neighbours.size(); ++n){
		Node current(neighbours[n]);
		vector<Node> neighbours2(current.findOutNeighbour(listNodes2, vecNodesGraph));
		if (neighbours2.empty()){ // end of the sequence
			if (not current.isRepeat){
				if (uint(current.SSNodes[0].second) == backbone.size() and current.solidity >= solidityThreshold){
					walk.push_back(current);
					found = true;
					break;
				} else {
					found = false;
				}
			} else {
				for (uint i(0); i < current.SSNodes.size(); ++i){
					if (uint(current.SSNodes[i].second) == backbone.size()){
						walk.push_back(current);
						found = true;
						break;
					} else {
						found = false;
					}
				}
			}
		}
		if (not found and not neighbours2.empty()){
			if (not current.isRepeat){
				if (current.solidity >= solidityThreshold){
					if (uint(current.SSNodes[0].second) == backbone.size() and current.seen < 3){
						walk.push_back(current);
						++vecNodesGraph[listNodes2.at(current.kmer)].seen;
						pushed = true;
						break;
					}
				}
			} else {
				for (uint i(0); i < current.SSNodes.size(); ++i){
					if (uint(current.SSNodes[i].second) == backbone.size() and current.seen < current.SSNodes.size() + 1){
						walk.push_back(current);
						++vecNodesGraph[listNodes2.at(current.kmer)].seen;
						pushed = true;
						break;
					}
				}
			}
		}
	 }
	 return pushed;
}


bool Graph::findNextNode(vector<Node>& walk, int& nextSS, uint solidityThreshold, unordered_set<uint>& seenRepeatsBtwSSN){
	bool pushed(false), found(false);
	vector<Node> possibleNext;
    // nb : solidity is not increased for repeated nodes so regular nodes have an advantage
	Node next(vecNodesGraph[listNodes2.at(backbone[nextSS])]);
	uint overlap(next.kmer.size() - 1);
	vector<Node> neighbours2;
	//~ cout << walk.back().kmer << " " ;
	//~ if (walk.back().isSSNode){
		//~ cout << walk.back().SSNodes[0].first + 1 << endl;
	//~ }
	//~ cout << endl;
	while (not found and not pushed and overlap > 3){  // change for more neighbours
			if (overlap == next.kmer.size() - 1){
				neighbours2 = walk.back().findOutNeighbour(listNodes2, vecNodesGraph);
			} else {
				neighbours2 = walk.back().findOutKNeighbour(listNodes2, overlap, vecNodesGraph);
			}
			//~ sort(neighbours2.begin(), neighbours2.end(), compareNodesBySolid);
			for (uint n(0); n < neighbours2.size(); ++n){
				
				Node current(neighbours2[n]);
				//~ cout << "possible neigh " << current.kmer << " " <<current.solidity<<  endl;
				if (current.kmer == next.kmer){  // we have found the next SS
					next.overlapToNext = overlap;
					walk.push_back(next);
					++nextSS;
					pushed = true;
					break;
				} else {
					if (not current.isRepeat){ // regular node
							if (current.SSNodes[0].second == nextSS){
								possibleNext.push_back(current);
								found = true;
							} else {
								//~ cout << "wrong SSN " <<  current.SSNodes[0].second  << " expected: " << nextSS << endl; 
							}
						
					} else {
						for (uint i(0); i < current.SSNodes.size(); ++i){
							if (current.SSNodes[i].second == nextSS){
								possibleNext.push_back(current);
								found = true;
							} else {
							//~ cout << "wrong SSN : repeat expected: " << nextSS << endl; 
						    }
						}
					}
				}
			}
		--overlap;
	}
	sort(possibleNext.begin(), possibleNext.end(), compareNodesBySolid);
	if (not possibleNext.empty() and not pushed){  // here we sort the possible next nodes by solidity to choose one
	// here we sort the possible next nodes by solidity to choose one
		//  todo instead of having a solidity of 1 in the repeated reads we could just penalize
		uint m(possibleNext.size() - 1);
		//~ uint m(0);
		bool pushable(false);
		//~ while (m < possibleNext.size() and not pushed){  // if the best node cannot be passed through we still want to try next in the list
		while (m >= 0 and not pushed){  // if the best node cannot be passed through we still want to try next in the list
			if (not (possibleNext[m].isRepeat)){  // we cannot pass through this node more than twice
				if (possibleNext[m].seen < 3){
					pushable = true;
				} else {
					//~ cout << "seen node too much " << possibleNext[m].kmer <<  endl;
				}
			} else {
				if (possibleNext[m].seen < possibleNext[m].solidity and not seenRepeatsBtwSSN.count(possibleNext[m].index)){
					pushable = true;
				}else {
					//~ cout << "seen repeated node too much " << possibleNext[m].kmer <<  " " << possibleNext[m].solidity << endl;
				}
			} 
			if (pushable){
				possibleNext[m].overlapToNext = overlap + 1;
				walk.push_back(possibleNext[m]);
				++vecNodesGraph[listNodes2.at(possibleNext[m].kmer)].seen;
				pushed = true;
				pushable = false;
				if (possibleNext[m].isRepeat){
					seenRepeatsBtwSSN.insert({possibleNext[m].index});
				}
				break;
			} 
			//~ ++m;
			--m;
		}  // this means that if two nodes have the same solidity we will choose the first in the list by default
		found = false;
	} else {
		if (not pushed and possibleNext.empty()){
			//~ cout << "no candidate found, broke" << endl;
		}
	}
	return pushed;
}




vector<string> Graph::produceConsensus(const string& queryRead, uint solidityThreshold, vector<vector<Node>>& walkNodes){
	vector<Node> walk;
	walk.push_back(vecNodesGraph[listNodes2.at(backbone[0])]);
	vector<string> fractionConsensus(backbone.size() -1);
	int nextSS(1), nextSScpy(1);
	bool pushed(true);
	uint bb(0), ovl;
	string consensus;
	unordered_set<uint> seenRepeatsBtwSSN;
	cout << "in" << endl;
	while(bb < backbone.size()){
		nextSScpy = nextSS;
		// todo: un noeud non répété peut etre traversé > 1 fois si plusieurs aretes entrantes => vraie condition sur le passage à travers un noeud ?
		// todo: solidité des kmers répétés ?
		pushed = findNextNode(walk, nextSS, solidityThreshold, seenRepeatsBtwSSN);
		if (nextSScpy != nextSS or not pushed){ // we reached a SS kmers or the walk terminated too early
			++bb;
			if (nextSScpy != nextSS){
				if (not walk.empty()){ // add the whole sequence of the first kmer
					consensus = walk[0].kmer;
				}
				for (uint n(1); n < walk.size(); ++n){
					ovl = walk[n-1].overlapToNext;
					consensus += walk[n].kmer.substr(ovl); 
				}
				for (Node nn : walk){
					vecNodesGraph[listNodes2.at(nn.kmer)].seen = 0;
				}
			} else { // we could not find a next kmer for some reason
				++nextSS;
				consensus = "";
			}
			fractionConsensus[nextSScpy - 1] = consensus;
			walkNodes[bb] = walk;
			if (uint(nextSS) == backbone.size() or walk.back().kmer == backbone.back()){  // we reached the last SS kmer of the backbone
				break;
			} else {
				walk = {};
				walk.push_back(vecNodesGraph[listNodes2.at(backbone[bb])]);
			}
			seenRepeatsBtwSSN.clear();
			
		}
	}
	cout << "fater while" << endl;
	//  before and after the first and last SS kmers
	string consensusBefore;
	vector<Node> neighFirstBB(vecNodesGraph[listNodes2.at(backbone[0])].findInNeighbour(listNodes2, sizeK, vecNodesGraph));
	if (not neighFirstBB.empty()){
		vector<Node> walkBefore;
		walkBefore.push_back(vecNodesGraph[listNodes2.at(backbone[0])]);
		pushed = true;
		while(pushed){
			pushed = getBeforeFirst(walkBefore, solidityThreshold);
		}
		walkNodes[0] = walkBefore;
		uint(j);
		for (int i(walkBefore.size()-1); i >= 0; --i){
			j = uint(i);
			if (j == walkBefore.size() - 1){
				consensusBefore += walkBefore[i].kmer;
			} else {
				ovl = walkBefore[i+1].overlapToNext;
				consensusBefore += walkBefore[i].kmer.substr(ovl);
			}
		}
	} else {  // first kmer of the backbone is also the beginning of the sequence 
		consensusBefore = "";
	}
	cout << "after bef" << endl;
	vector<Node> neighLastBB(vecNodesGraph[listNodes2.at(backbone.back())].findOutNeighbour(listNodes2, vecNodesGraph));
	string consensusAfter;
	if (not neighLastBB.empty() and walk.back().kmer == backbone.back()){
		vector<Node> walkAfter;
		walkAfter.push_back(vecNodesGraph[listNodes2.at(backbone.back())]);
		pushed = true;
		while(pushed){
			pushed = getAfterLast(walkAfter, solidityThreshold);
		}
		if (not walkAfter.empty()){
			for (uint n(0); n < walkAfter.size(); ++n){
				if (n==0){
					consensusAfter += walkAfter[n].kmer;
				} else {
						ovl = walkAfter[n-1].overlapToNext;
						consensusAfter += walkAfter[n].kmer.substr(ovl);  // warning this wont be ok with k-2 neighbours
				}
			}
		} else {
			consensusAfter = "";
		}
		walkNodes[backbone.size()] = walkAfter;
	} else {
		consensusAfter = "";
	}
	cout << "after af" << endl;
	vector<string> toReturn;
	toReturn.push_back(consensusBefore);
	for (uint c(0); c < fractionConsensus.size(); ++c){
		toReturn.push_back(fractionConsensus[c]);
	}
	toReturn.push_back(consensusAfter);
	return toReturn;
}




// if  a backbone kmer overlaps the next one, store the maximal overlap between the two. If there is an overlap like k1 -> k2 -> k3, also remember that k1 is linked to k3
unordered_map<string, vector<pair<uint, uint>>>	 compactBackbone(vector<string>& backbone, uint sizeK, uint minOverlap = MIN_OVERLAP){
	unordered_map<string, vector<pair<uint, uint>>>	backboneToOverlap;
	bool found(false);
	uint pos0(sizeK - minOverlap), len1(minOverlap), len;
	for (uint i(0); i < backbone.size() - 1; ++i){
		found = false;
		pos0 = sizeK - minOverlap;
		len1 = minOverlap;
		while (pos0 > 0){
			if (backbone[i].substr(pos0) == backbone[i+1].substr(0, len1)){
				found = true;
				len = len1;
			}
			-- pos0; ++len1;
		}
		if (found){
			
			if (i > 0){
				uint j = i;
				while (j > 0){
					if (backboneToOverlap.count(backbone[j-1])){
						if (backboneToOverlap[backbone[j-1]].back().first == i){
							backboneToOverlap[backbone[j-1]].push_back({i+1, 0});
							backboneToOverlap[backbone[i+1]].push_back({j-1, 0});
							--j;
						} else {
							break;
						}
					} else {
						break;
					}
				}
			}
			backboneToOverlap[backbone[i]].push_back({i+1, len});
			backboneToOverlap[backbone[i+1]].push_back({i, len}); // kmers added in order
		}
	}
	return backboneToOverlap;
}






string Graph::getConsensusString(const uint index1, const uint index2, const vector<string>& vecCons, const string& queryRead){
	string consensus ("");
	bool broken(false);
	if (index1 == index2){
		consensus += vecCons[index1];
	} else {
		for (uint i(index1); i < index2; ++i){
			if (vecCons[i].empty()){
					if(i != 0){
						broken = true;
						break;
					}
			} else {
				consensus += vecCons[i].substr(0, vecCons[i].size() - sizeK);
			}
		}
	}
	if (not broken){
			consensus += vecCons[index2];
	}
	if (consensus.size() > queryRead.size()/2){
		return consensus;
	} else {
		cout << "Could not correct, throwing back the query read" << endl;
		return queryRead;
	}
}


string getConsensusStringBetweenSSN(const uint index1, const uint index2, vector<string>& backbone, const vector<string>& vecCons, const uint k, const string& queryRead){
	string consensus ("");
	bool broken(false);
	//index 1 and 2 are indexes in vecCons
	if (index1 == index2){
		if (index1 == 0){
			if (not vecCons[index1].empty()){
				if (vecCons[index1].substr(vecCons[index1].size() - k) == backbone[0]){
					consensus += vecCons[index1];
				}
			}
		} else if (index1 == vecCons.size() - 1){
			if (not vecCons[index1].empty()){
				if (vecCons[index1].substr(0, k) == backbone.back()){
					consensus += vecCons[index1];
				}
			}
		} else {
			if (not vecCons[index1].empty()){
				if (vecCons[index1].substr(0, k) == backbone[index1 - 1] and vecCons[index2].substr(vecCons[index2].size() - k) == backbone[index2]){
					consensus += vecCons[index1];
				} else {
					broken = true;
				}
			} else {
				broken = true;
			}
		}
	} else {
		uint i(index1);
		while (i <= index2){
			if(i != 0 and i !=  vecCons.size() - 1){
				if (vecCons[i].empty()){
					broken = true;
					break;
				}
			}
			if(not broken) {
				if (i != 0){
					if (i != vecCons.size() - 1){
						if (not (vecCons[i].substr(0, k) == backbone[i - 1] and vecCons[i].substr(vecCons[i].size() - k) == backbone[i])){
							broken = true;
							break;
						} else {
							consensus += vecCons[i].substr(0, vecCons[i].size() - k);
						}
					} else {
						if (not (vecCons[i].substr(0, k) == backbone[i-1])){
							consensus +=  vecCons[i-1].substr(vecCons[i-1].size() - k);
						} else {
							if (vecCons[i].size() > k){
								consensus += vecCons[i].substr(0, vecCons[i].size() - k);
							} else if (vecCons[i].size() == k){
								consensus += vecCons[i];
							}
						}
					}
				} else {
					if (vecCons[i].size() > k){
						if (vecCons[i].substr(vecCons[i].size() - k) == backbone[0]){
							consensus += vecCons[i].substr(0, vecCons[i].size() - k);
						}
					}
				}
			}
			++i;
		}
		if (not broken){
			if (i !=  vecCons.size() and i != 0){
				consensus += vecCons[i-1].substr(vecCons[i-1].size() - k);
			}
		} else {
			consensus = "";
		}
	}
	
	return consensus;
}





void correctQuery(vector<string>& vecCons, vector<string>& vecReads, vector<string>& newVecReads, vector<vector<uint>>& SSKmersPerRead, uint indexQuery, uint k, vector<string>& backbone){
	string newQuery("");
	string query(vecReads[indexQuery]);
	uint index1, index2;
	// get indexes
	if (SSKmersPerRead[indexQuery].size() > 1){
		if (SSKmersPerRead[indexQuery][0] == 0){
			index1 = 0;
		} else {
			index1 = SSKmersPerRead[indexQuery][0] + 1;
		}
		if (SSKmersPerRead[indexQuery].back() == backbone.size() - 1){
			index2 =  backbone.size();
		} else {
			index2 = SSKmersPerRead[indexQuery].back();
		}
		string consensus(getConsensusStringBetweenSSN(index1, index2, backbone, vecCons, k, vecReads[indexQuery]));
		// If we can find a large zone between   the first SSN found in the query and he last SSN found in the query
		if (false){
			uint i(0);
			while (i < query.size() - k + 1){ // beginning of the sequence before the first SSN found in the query
				if (query.substr(i, k) == consensus.substr(0, k)){
					break;
				} else {
					newQuery.push_back(query[i]);
					++i;
				}
			}
			for (uint j(0); j < consensus.size(); ++j){ // corrected part
				newQuery.push_back(consensus[j]);
			}
			bool scdSSNpassed(false);
			for (uint jj(i); jj < query.size() - k + 1; ++jj){ // rest of the sequence after the last SSN found in the query
				if (scdSSNpassed){
					newQuery.push_back(query[jj]);
				}
				if (query.substr(jj, k) == consensus.substr(consensus.size() - k)){
					scdSSNpassed = true;
				}
			}
		} else { // correct by fragments
			uint i(0), posiInQ(0);
			while (i < SSKmersPerRead[indexQuery].size() - 1){
				if (SSKmersPerRead[indexQuery][i] == 0){
					index1 = 0;
				} else {
					index1 = SSKmersPerRead[indexQuery][i] + 1;
				}
				if (SSKmersPerRead[indexQuery][i] == backbone.size() - 1){
					index2 =  backbone.size();
				} else {
					index2 = SSKmersPerRead[indexQuery][i+1];
				}
				if (i==0){
					if (index1 == 0){
						consensus = getConsensusStringBetweenSSN(0, 0, backbone, vecCons, k, vecReads[indexQuery]);
						if (not consensus.empty()){
							newQuery += consensus;
							while (query.substr(posiInQ, k) != backbone[SSKmersPerRead[indexQuery][i]]){
								++posiInQ;
							}
						} else {
							while (query.substr(posiInQ, k) != backbone[SSKmersPerRead[indexQuery][i]]){
								newQuery.push_back(query[posiInQ]);
								++posiInQ;
							}
						}
						
					} else {
						consensus = getConsensusStringBetweenSSN(0, SSKmersPerRead[indexQuery][i], backbone, vecCons, k, vecReads[indexQuery]);
						if (not consensus.empty()){
							newQuery += consensus;
							while (query.substr(posiInQ, k) != backbone[SSKmersPerRead[indexQuery][i]]){
								++posiInQ;
							}
						} else {
							while (query.substr(posiInQ, k) != backbone[SSKmersPerRead[indexQuery][i]]){
								newQuery.push_back(query[posiInQ]);
								++posiInQ;
							}
						}
					}
				}
				consensus = getConsensusStringBetweenSSN(index1, index2, backbone, vecCons, k, vecReads[indexQuery]);
				if (not consensus.empty()){
						if (not newQuery.empty()){
							if (newQuery.size() >= k){
								if (newQuery.substr(newQuery.size() - k) == consensus.substr(0, k)){
									newQuery += consensus.substr(k);
								} else {
								newQuery += consensus;
								}
							} else {
								newQuery += consensus;
							}
						} else {
							newQuery += consensus;
						}
						while (posiInQ < query.size() and query.substr(posiInQ, k) != backbone[SSKmersPerRead[indexQuery][i+1]]){
							++posiInQ;
						}
				} else {
					string kmerL;
					for (uint kk(0); kk < k ; ++kk){
								kmerL.push_back(query[posiInQ]);
								++posiInQ;
					}
					if (not newQuery.empty()){
						if (kmerL != newQuery.substr(newQuery.size() - k)){
							for (uint kk(0); kk < kmerL.size() ; ++kk){
								newQuery.push_back(kmerL[kk]);
							}
						}
					} else {
						for (uint kk(0); kk < kmerL.size() ; ++kk){
								newQuery.push_back(kmerL[kk]);
						}
					}
					while (posiInQ < query.size() and query.substr(posiInQ, k) != backbone[SSKmersPerRead[indexQuery][i+1]]){
						newQuery.push_back(query[posiInQ]);
						++posiInQ;
					}
				}
				++i;
			}
			if (SSKmersPerRead[indexQuery][i] == backbone.size() - 1){
					index2 =  backbone.size();
					consensus = getConsensusStringBetweenSSN(index2, index2, backbone, vecCons, k, vecReads[indexQuery]);
					if (not consensus.empty()){
						if (not newQuery.empty()){
							if (newQuery.substr(newQuery.size() - k) == consensus.substr(0, k)){
								newQuery += consensus.substr(k);
							}
							
						} else {
							newQuery += consensus;
						}
					} else {
						if (posiInQ + k <= query.size()){
							string kmerL;
							for (uint kk(0); kk < k ; ++kk){
								kmerL.push_back(query[posiInQ]);
								++posiInQ;
							}
							if (kmerL != newQuery.substr(newQuery.size() - k)){
								for (uint kk(0); kk < kmerL.size() ; ++kk){
									newQuery.push_back(kmerL[kk]);
								}
							}
							while (posiInQ < query.size()){
								newQuery.push_back(query[posiInQ]);
								++posiInQ;
							}
						}
					}
			} else {
				consensus = getConsensusStringBetweenSSN(SSKmersPerRead[indexQuery][i] + 1, backbone.size(), backbone, vecCons, k, vecReads[indexQuery]);
				if (not consensus.empty()){
					if (not newQuery.empty()){
						if (newQuery.substr(newQuery.size() - k) == consensus.substr(0, k)){
							newQuery += consensus.substr(k);
						}
						
					} else {
						newQuery += consensus;
					}
				} else {
					if (posiInQ + k <= query.size()){
						string kmerL;
						for (uint kk(0); kk < k ; ++kk){
							kmerL.push_back(query[posiInQ]);
							++posiInQ;
						}
						if (kmerL != newQuery.substr(newQuery.size() - k)){
							for (uint kk(0); kk < kmerL.size() ; ++kk){
								newQuery.push_back(kmerL[kk]);
							}
						}
						while (posiInQ < query.size()){
							newQuery.push_back(query[posiInQ]);
							++posiInQ;
						}
					}
				}
			}
		}
	}
	
	if (not newQuery.empty()){
		newVecReads[indexQuery] = newQuery;
	} else {
		newVecReads[indexQuery] = vecReads[indexQuery];
	}
}
// todo : add before and after the first and last SSN





	
void correctData(ifstream& readFile, const uint ksize, const uint solidity){
	ofstream out("corrected_reads.fa");
	vector<string> vecReads;
	vector<string> backbone; // = {"TATTCTTTT", "CCAAAGTGT"};
	vector<vector<uint>> SSKmersPerRead;  // = {{0,1}, {1}, {0,1}, {0} , {0}, {0,1}, {1}};
	vector<uint> distancesSSN;  // = {{0,1}, {1}, {0,1}, {0} , {0}, {0,1}, {1}};
	bool continu(processReads(readFile, ksize, solidity, backbone, SSKmersPerRead, distancesSSN, vecReads));
	if (continu){
		//~ cout << continu << endl;
		//~ readDataset(readFile, vecReads);
		//~ vector<string> newReads(vecReads.size());
		uint k(ksize);
		uint solidityT(solidity);
		cout << "started graph construction..." << endl;
		Graph graph(vecReads, SSKmersPerRead, backbone, k);
		cout << "graph update..." << endl;
		graph.updateNodes();
		//~ cout << "research of a consensus..." << endl;
		//~ vector<vector<Node>> walkNodes(backbone.size() + 1);
		//~ cout << "ok" << endl;
		//~ vector<string> consensusV(graph.produceConsensus(vecReads[0], solidityT, walkNodes));
		//~ cout << "okk" << endl;
		//~ ofstream fileO("graph3.gfa");
		//~ toGfaWalk(graph, k, solidityT, walkNodes, fileO);
		//~ cout << "correction of the reads..." << endl;
		//~ for (uint i(0); i < vecReads.size(); ++i){
			//~ correctQuery(consensusV, vecReads, newReads, SSKmersPerRead, i, k, backbone);
		//~ }
		//~ cout << consensusV.size() << endl;
		//~ for (uint i(0); i < consensusV.size(); ++i){
			//~ cout << i << " " << consensusV[i] << endl;
		//~ }
		//~ uint i(0);
		//~ for (string s : newReads){
			//~ out << ">read_" << i << endl;
			//~ out << s << endl;
			//~ ++i;
		//~ }
		//~ string consensus(graph.getConsensusString(0, consensusV.size()-1, consensusV,  vecReads[0]));
		//~ ofstream outc("consensus.fa");
		//~ outc << ">consensus|size_" <<  consensus.size() << endl;
		//~ outc << consensus << endl;
	} else {
		cout << "something went wrong with the backbone file, maybe empty ?" << endl;
	}
}



bool compareNodesBySolid(const Node& node1, const Node& node2){
	return node1.solidity < node2.solidity;
}

