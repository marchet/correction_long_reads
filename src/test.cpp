#include <vector>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <unordered_set>
#include "consensus.hpp"	
#include "test.hpp"	




using namespace std;

// test read processing

bool test_getWindowsInQuery(){
	bool passed(true);
	uint indexQuery(0), k(3), w(5);
	vector<string> vecReads{"ACGTAGTGTTAGCGGATGTTACA"};
	vector<string> backbone{"ACG", "CGT", "TGT", "TAG", "GGA", "ACA"};
	vector<vector<uint>> SSNperRead{{0,1,2,3,4,5}};
	vector<pair<uint, uint>> listSSKWindows;
	vector<pair<int, int>> listPairsSSKWindows;
	//~ getWindowsInQuery(indexQuery, vecReads, backbone, SSNperRead, k, w, listSSKWindows, listPairsSSKWindows);

	if (not (listSSKWindows.size() == 4)){
		passed = false;
	} else {
		if (not (listSSKWindows[0].first == 0 and listSSKWindows[0].second == 0 and listSSKWindows[1].first == 2 and listSSKWindows[1].second == 6 and listSSKWindows[2].first == 4 and listSSKWindows[2].second == 13 and listSSKWindows[3].first == 5 and listSSKWindows[3].second == 20)){
			passed = false;
		}
	}
	if (listPairsSSKWindows.size() != 3){
		passed = false;
	} else {
		if (not(listPairsSSKWindows[0].first == 0  and listPairsSSKWindows[0].second == 2 and listPairsSSKWindows.back().first == 4  and listPairsSSKWindows.back().second == 5)){
			passed = false;
		}
	}
	return passed;
}

//~ bool test_getWindowsInOthers(){
	//~ bool passed(true);
	//~ uint indexQuery(0), k(3), w(5);
	//~ vector<string> vecReads{"ACGTAGTGTTAGCGGATGTTACA", "AACGTAGTTTAGCGGATGTTAC", "CGTAGTGTTAGCAGGATGTTACA"};
	//~ vector<string> backbone{"ACG", "CGT", "TGT", "TAG", "GGA", "ACA"};
	//~ vector<vector<uint>> SSNperRead{{0,1,2,3,4,5}, {0,1,3,4}, {1,2,3,4,5}};
	//~ vector<pair<uint, uint>> listSSKWindows;
	//~ vector<pair<int, int>> listPairsSSKWindows;
	//~ vector<vector<pair<int, int>>> listWindows;
	//~ getWindowsInQuery(indexQuery, vecReads, backbone, SSNperRead, k, w, listSSKWindows, listPairsSSKWindows);
	//~ getWindowsInOthers(indexQuery, vecReads, backbone, SSNperRead, listPairsSSKWindows, listWindows);
	//~ if (not (listWindows[1][0].first  == 0 and listWindows[1][0].second == 3 and listWindows[1][1].first  == 0 and listWindows[1][1].second == 4 and listWindows[2][1].first  == 2 and listWindows[2][1].second == 4 and listWindows[2][2].first  == 4 and listWindows[2][2].second == 5)){
		//~ passed = false;
	//~ }
	//~ return passed;
	
//~ }



//~ bool test_chopReadsInWindows(){
	//~ bool passed(true);
	//~ uint indexQuery(0), k(3), w(5);
	//~ vector<string> vecReads{"ACGTAGTGTTAGCGGATGTTACA", "AACGTAGTTTAGCGGATGTTAC", "CGTAGTGTTAGCAGGATGTTTACA"};
	//~ vector<string> backbone{"ACG", "CGT", "TGT", "TAG", "GGA", "ACA"};
	//~ vector<vector<uint>> SSNperRead{{0,1,2,3,4,5}, {0,1,3,4}, {1,2,3,4,5}};
	//~ vector<pair<uint, uint>> listSSKWindows;
	//~ vector<pair<int, int>> listPairsSSKWindows;
	//~ vector<vector<pair<int, int>>> listWindows;
	//~ getWindowsInQuery(indexQuery, vecReads, backbone, SSNperRead, k, w, listSSKWindows, listPairsSSKWindows);
	//~ getWindowsInOthers(indexQuery, vecReads, backbone, SSNperRead, listPairsSSKWindows, listWindows);
	//~ vector<string> subseqVec;
	//~ chopReadsInWindows(listSSKWindows, k, subseqVec, listWindows, 2, 0, vecReads, backbone, SSNperRead);
	//~ chopReadsInWindows(listSSKWindows, k, subseqVec, listWindows, 0, 0, vecReads, backbone, SSNperRead);
	//~ chopReadsInWindows(listSSKWindows, k, subseqVec, listWindows, 1, 0, vecReads, backbone, SSNperRead);
	//~ if (not (subseqVec[0] == "GGATGTTACA" and subseqVec[1] == "GGATGTTTACA" and subseqVec[2] == "ACGTAGTGT" and subseqVec[3] == "ACGTAG"  and subseqVec[4] == "TGTTAGCGGA" and subseqVec[5] == "ACGTAGTTTAGCGGA" and subseqVec[6] == "TGTTAGCAGGA")){
		//~ passed = false;
	//~ }
	//~ return passed;
	
//~ }


//~ bool test_getWindowsOnReads(){
	//~ bool passed =true;
	//~ uint k(3), w(5), s(2);
	//~ vector<string> vecReads{"ACGTAGTGTTAGCGGATGTTACA", "AACGTAGTTTAGCGGATGTTAC", "CGTAGTGTTAGCAGGATGTTTACA"};
	//~ vector<string> backbone{"ACG", "CGT", "TGT", "TAG", "GGA", "ACA"};
	//~ vector<vector<uint>> SSNperRead{{0,1,2,3,4,5}, {0,1,3,4}, {1,2,3,4,5}};
	//~ getWindowsOnReads(vecReads,  backbone, SSNperRead,  k,  w, s);
	//~ return passed;
//~ }








// tests on Node objects


bool test_NodeConstrCopy(){
	bool passed(false);
	Node test(0, "A", 0, 1, 2, false);
	Node test2(test);
	passed = (test.index == test2.index) * (test.kmer == test2.kmer) * (test.SSNodes[0].first == test2.SSNodes[0].first) *(test.SSNodes[0].second == test2.SSNodes[0].second) * (test.isRepeat == test2.isRepeat) * (test.isSSNode == test2.isSSNode) * (test.solidity == test2.solidity) ;
	return passed;
}



bool test_NodeConstr(){
	bool passed(false);
	Node test(0, "A", 0, 1, 2, false);
	passed = (test.index == 0) * (test.kmer == "A") * (test.SSNodes[0].first == 1) *(test.SSNodes[0].second == 2) * (test.isRepeat == false) * (test.isSSNode == false) * (test.solidity == 1) ;
	return passed;
}



bool test_NodeConstrSSN(){
	bool passed(false);
	Node test(0, "A", 1);
	passed = (test.index == 0) * (test.kmer == "A") * (test.SSNodes[0].first == 0) * (test.SSNodes[0].second == 2) * (test.isRepeat == false) * (test.isSSNode == true) * (test.solidity == 1) ;
	return passed;
}


bool test_updateRegular(){
	bool passed(false);
	Node test(0, "A", 0, 1, 3, false);
	test.update(1, 2, false);
	test.update(1, 3, false);
	passed = (test.index == 0) * (test.kmer == "A") * (test.SSNodes[0].first == 1) * (test.SSNodes[0].second == 2) * (test.isRepeat == false) * (test.isSSNode == false) * (test.solidity == 3) ;
	return passed;
}


bool test_updateRegularToRepeat(){
	bool passed(false);
	Node test(0, "A", 0, 1, 3, false);
	test.update(15, 16, false);
	passed = (test.isRepeat == true) * (test.SSNodes.size() == 2) * (test.solidity == 3) ;
	return passed;
}



bool test_updateRepeat(){
	bool passed(false);
	Node test(0, "A", 0, 2, 3, true);
	test.update(3, 4, true);
	test.updateRepeat();
	passed = (test.index == 0) * (test.kmer == "A") * (test.SSNodes.size() == 2) * (test.isRepeat == true) * (test.isSSNode == false) * (test.solidity == 1) ;
	return passed;
}

bool test_changeSSNRegular(){
	uint k = 3;
	unordered_map<string, uint> listNodes2;
	bool passed(false);
	Node test(0, "CAA", 0, 2, 3, false);
	Node prev1(1, "TCA", 0, 1, 4, false);
	vector<Node>vecNodesGraph = {test, prev1};
	listNodes2.insert({"CAA", 0}); listNodes2.insert({"TCA", 1});
	test.changeSSN(listNodes2, k, vecNodesGraph);
	Node prev2(1, "GCA", 0, 3, 4, false);
	vecNodesGraph = {test, prev1, prev2};
	listNodes2.insert({"GCA",2}); 
	vecNodesGraph[listNodes2.at("CAA")].changeSSN(listNodes2, k, vecNodesGraph);
	passed = (vecNodesGraph[listNodes2.at("TCA")].SSNodes[0].second == 3);
	return passed;
}


bool test_findOutKNeighbour(){
	bool passed(false);
	unordered_map<string, uint> list;
	Node test(0, "ACTTG", 0, 3, 6, false);
	Node next(1, "TTGAA", 0, 3, 6, false);
	Node next2(0, "TTGCA", 0, 3, 6, false);
	Node next3(0, "TGCCC", 0, 3, 6, false);
	vector<Node> nn ({test, next, next2, next3});
	list.insert({"ACTTG", 0});
	list.insert({"TTGAA", 1});
	list.insert({"TTGCA", 2});
	list.insert({"TGCCC", 3});
	vector<Node> neighbours(test.findOutKNeighbour(list, 3, nn));
	passed = (neighbours.size() == 2);
	for (Node n : neighbours){
		if (n.kmer != "TTGCA" and n.kmer != "TTGAA"){
			passed = false;
			break;
		}
	}
	return passed;
}



bool test_findOutKnNeighbour(){
	bool passed(false);
	unordered_map<string, uint> list;
	Node test(0, "ACTTG", 0, 3, 6, false);
	Node next(1, "TTGAA", 0, 3, 6, false);
	Node next2(0, "TTGCA", 0, 3, 6, false);
	Node next3(0, "TGCCC", 0, 3, 6, false);
	Node next4(0, "CTTGT", 0, 3, 6, false);
	vector<Node> nn ({test, next, next2, next3, next4});
	list.insert({"ACTTG", 0});
	list.insert({"TTGAA", 1});
	list.insert({"TTGCA", 2});
	list.insert({"TGCCC", 3});
	list.insert({"CTTGT", 4});
	vector<Node> neighbours;
	uint i(4);
	while (i > 1){
		vector<Node> v(test.findOutKNeighbour(list, i, nn));
		for (Node n : v){
			neighbours.push_back(n);
		}
		--i;
	}
	passed = (neighbours.size() == 4);
	for (Node n : neighbours){
		if (n.kmer != "TTGCA" and n.kmer != "TTGAA" and n.kmer != "TGCCC" and n.kmer != "CTTGT" ){
			passed = false;
			break;
		}
	}
	return passed;
}


bool test_findOutNeighbour(){
	bool passed(false);
	unordered_map<string, uint> list;
	Node test(0, "ACT", 0, 3, 6, false);
	Node test2(0, "ACG", 0, 3, 6, false);
	Node prev(0, "AAC", 0, 3, 6, false);
	vector<Node> nn({test, test2});
	list.insert({"ACT", 0});
	list.insert({"ACG", 1});
	vector<Node> neighbours(prev.findOutNeighbour(list, nn));
	vector<Node> neighbours2(test.findOutNeighbour(list, nn));
	passed = (neighbours.size() == 2)  * (neighbours2.empty()) ;
	return passed;
}

bool test_findInNeighbour(){
	bool passed(false);
	uint k(3);
	unordered_map<string, uint> list;
	Node test(0, "TAT", 0, 3, 6, false);
	Node prev(0, "CTA", 0, 3, 6, false);
	Node prev2(0, "GTA", 0, 3, 6, false);
	vector<Node> nn({test, prev, prev2});
	list.insert({"CTA", 1});list.insert({"TAT", 0}); list.insert({"GTA", 2});
	vector<Node> neighbours(test.findInNeighbour(list, k, nn));
	passed = (neighbours.size() == 2);
	for (Node n : neighbours){
		if (n.kmer != "GTA" and n.kmer != "CTA"){
			passed = false;
			break;
		}
	}
	return passed;
}




// test on object Graph


bool test_GraphConstr(){
	bool passed(true);
	vector<string> vecReads;
	vector<vector<uint>> SSKmersPerRead;
	
	uint k(3);
	                //  ACTAGGATTC
	vecReads.push_back("ACTAGGATAC"); // *ACT - CTA - TAG - AGG - GGA - GAT - ATA - TAC
	vecReads.push_back("ACAAGGTTC"); // ACA - CAA - AAG - (AGG) - GGT - GTT - *TTC
	SSKmersPerRead.push_back({0});
	SSKmersPerRead.push_back({1});
	vector<string> backbone = {"ACT", "TTC"};
	Graph graph(vecReads, SSKmersPerRead, backbone, k);

	passed *= (graph.listNodes2.size() == 14);
	for (auto it(graph.listNodes2.begin()); it != graph.listNodes2.end(); ++it){
		if (it->first == "ACT"){
			passed *= (graph.vecNodesGraph[it->second].solidity == 1) *(graph.vecNodesGraph[it->second].isSSNode == true );
		}
		if (it->first == "AGG"){
			passed *= (graph.vecNodesGraph[it->second].solidity == 2 ) *(graph.vecNodesGraph[it->second].isSSNode == false ) * (graph.vecNodesGraph[it->second].SSNodes[0].second == 1);
		}
		if (it->first == "TAC"){
			passed *= (graph.vecNodesGraph[it->second].SSNodes[0].second == 2);
		}
		if (not passed) {
			break;
		}
	}
	return passed;
}


// todo do a real update
bool test_GraphUpdate(){
	bool passed(true);
	vector<string> vecReads;
	vector<vector<uint>> SSKmersPerRead;
	
	uint k(3);
	                //  ACTAGGATTC
	vecReads.push_back("ACTAGGATAC"); // *ACT - CTA - TAG - AGG - GGA - GAT - ATA - TAC
	vecReads.push_back("ACAAGGTTC"); // ACA - CAA - AAG - (AGG) - GGT - GTT - *TTC
	SSKmersPerRead.push_back({0});
	SSKmersPerRead.push_back({1});
	vector<string> backbone = {"ACT", "TTC"};
	Graph graph(vecReads, SSKmersPerRead, backbone, k);
	graph.updateNodes();
	passed *= (graph.listNodes2.size() == 14);
	for (auto it(graph.listNodes2.begin()); it != graph.listNodes2.end(); ++it){
		
		if (it->first == "ACT"){
			passed *= (graph.vecNodesGraph[it->second].solidity == 1) *(graph.vecNodesGraph[it->second].isSSNode == true );
		}
		if (it->first == "AGG"){
			passed *= (graph.vecNodesGraph[it->second].solidity == 2 ) *(graph.vecNodesGraph[it->second].isSSNode == false ) * (graph.vecNodesGraph[it->second].SSNodes[0].second == 1);
		}
		if (it->first == "ACA"){
			passed *= (graph.vecNodesGraph[it->second].SSNodes[0].first == 0);
		}
		if (not passed) {
			break;
		}
	}
	return passed;
}




bool test_getConsensusString(){
	bool passed(false);
	//real seq AGGTACACCTTGAGTACATGGCGTTTA
	vector<string> backbone({"AGGT", "CCTT", "ACAT","TTTA"});
	uint k(4);
	vector<string> vecCons({"CCAGGT", "AGGTA", "CCTTGAGTACAT", "ACATGGCGTTTA", ""});
	vector<string> reads({"AGGTACACCTTGTGTACATGGCGTTTA"});
	string consensus(getConsensusStringBetweenSSN(1, 1, backbone, vecCons, k, reads[0]));
	if (consensus == ""){
		passed = true;
	}
	consensus = getConsensusStringBetweenSSN(0, 0, backbone, vecCons, k, reads[0]);
	passed = (consensus == "CCAGGT");
	consensus = getConsensusStringBetweenSSN(0, 1, backbone, vecCons, k, reads[0]);
	passed *= (consensus == "");
	consensus = getConsensusStringBetweenSSN(1, 2, backbone, vecCons, k, reads[0]);
	passed *= (consensus == "");
	consensus = getConsensusStringBetweenSSN(2, 3, backbone, vecCons, k, reads[0]);
	passed *= (consensus == "CCTTGAGTACATGGCGTTTA");
	consensus = getConsensusStringBetweenSSN(4, 4, backbone, vecCons, k, reads[0]);
	passed *= (consensus == "");
	consensus = getConsensusStringBetweenSSN(3, 4, backbone, vecCons, k, reads[0]);
	passed *= (consensus == "ACATGGCGTTTA");
	return passed;
}


bool test_getConsensusString2(){
	bool passed(false);
	//real seq AGGTACACCTTGAGTACATGGCGTTTA
	vector<string> backbone({"AGGT", "CCTT", "ACAT","TTTA"});
	uint k(4);
	vector<string> vecCons({"CCAGGTA", "AGGTACACCTT", "", "ACATGGCGTTTA", "TTTA"});
	vector<string> reads({"AGGTACACCTTGTGTACATGGCGTTTA"});
	string consensus(getConsensusStringBetweenSSN(0, 4, backbone, vecCons, k, reads[0]));
	if (consensus == ""){
		passed = true;
	}
	consensus = getConsensusStringBetweenSSN(1, 3, backbone, vecCons, k, reads[0]);
	passed *= (consensus == "");
	consensus = getConsensusStringBetweenSSN(3, 4, backbone, vecCons, k, reads[0]);
	passed *= (consensus == "ACATGGCGTTTA");
	consensus = getConsensusStringBetweenSSN(4, 4, backbone, vecCons, k, reads[0]);
	passed *= (consensus == "TTTA");
	return passed;
}



bool test_getConsensusString3(){
	bool passed(false);
	//real seq AGGTACACCTTGAGTACATGGCGTTTA
	vector<string> backbone({"AGGT", "CCTT", "ACAT","TTTA"});
	uint k(4);
	vector<string> vecCons({"", "AGGTACACCTT", "", "ACATGGCGTTTA", "TTTA"});
	vector<string> reads({"AGGTACACCTTGTGTACATGGCGTTTA"});
	string consensus(getConsensusStringBetweenSSN(0, 1, backbone, vecCons, k, reads[0]));
	if (consensus == "AGGTACACCTT"){
		passed = true;
	}
	return passed;
}



bool test_compactBackbone(){
	bool passed(true);
	vector<string> backbone({"AGGT", "GGTA", "TACC", "ACAT","ATCA"});
	uint k(4);
	unordered_map<string, vector<pair<uint, uint>>> mapp(compactBackbone(backbone, k, 2));
	if (not (mapp.count("AGGT"))){
		passed = false;
	} else {
		if (not (mapp["AGGT"][0].first == 1 and mapp["AGGT"][0].second == 3)){
			passed = false;
		}
		if (not (mapp["AGGT"][1].first == 2 and mapp["AGGT"][1].second == 0)){
			passed = false;
		}
	}
	if (not mapp.count("TACC")){
		passed = false;
	} else {
		if (not (mapp["TACC"].size() == 2) and mapp["TACC"][0].first == 0){
			passed = false;
		}
	}
	if (not (mapp.count("ACAT"))){
			passed = false;
	} else {
		if (not (mapp["ACAT"][0].first == 4 and mapp["ACAT"][0].second == 2)){
			passed = false;
		}
	}
	return passed;
}


bool test_correctQuery(){
	bool passed(false);
	//real seq AGGTACACCTTGAGTACATGGCGTTTA
	vector<string> backbone({"AGGT", "CCTT", "ACAT","TTTA"});
	uint k(4);
	vector<string> vecCons({"", "AGGTACACCTT", "CCTTGAGTACAT", "ACATGGCGTTTA", ""});
	vector<string> reads({"AGGTACACCTTGTGTACATGGCGTTTA"});
	vector<string> newReads({""});
	vector<vector<uint>> SSKmersPerRead({{0, 1, 2, 3}});
	correctQuery(vecCons, reads, newReads, SSKmersPerRead, 0, k, backbone);
	if(newReads[0] == "AGGTACACCTTGAGTACATGGCGTTTA"){
		passed = true;
	}
	return passed;
}



bool test_correctQuery2(){
	bool passed(false);
	//real seq AGGTACACCTTGAGTACATGGCGTTTA
	vector<string> backbone({"AGGT", "CCTT", "ACAT","TTTA"});
	uint k(4);
	vector<string> vecCons({"", "", "CCTTGAGTACAT", "ACATGGCGTTTA", ""});
	vector<string> reads({"AGGTACACCTTGTGTACATGGCGTTTA"});
	vector<string> newReads({""});
	vector<vector<uint>> SSKmersPerRead({{0, 1, 2, 3}});
	string consensus(getConsensusStringBetweenSSN(1, 1, backbone, vecCons, k, reads[0]));
	correctQuery(vecCons, reads, newReads, SSKmersPerRead, 0, k, backbone);
	if(newReads[0] == "AGGTACACCTTGAGTACATGGCGTTTA"){
		passed = true;
	}
	return passed;
}


bool test_correctQuery3(){
	bool passed(false);
	//real seq AGGTACACCTTGAGTACATGGCGTTTA
	vector<string> backbone({"AGGT", "CCTT", "ACAT","TTTA"});
	uint k(4);
	vector<string> vecCons({"", "", "CCTTGAGTACAT", "", ""});
	vector<string> reads({"AGGTACACCTTGTGTACATGGCGTTTA"});
	vector<string> newReads({""});
	vector<vector<uint>> SSKmersPerRead({{0, 1, 2, 3}});
	string consensus(getConsensusStringBetweenSSN(1, 1, backbone, vecCons, k, reads[0]));
	correctQuery(vecCons, reads, newReads, SSKmersPerRead, 0, k, backbone);
	if(newReads[0] == "AGGTACACCTTGAGTACATGGCGTTTA"){
		passed = true;
	}
	return passed;
}




bool test_correctQuery4(){
	bool passed(false);
	vector<string> backbone({"AGGT", "CCTT", "ACAT","TTTA"});
	uint k(4);
	vector<string> vecCons({"", "AGGTACACCTT", "CCTTGAGTACAT", "", ""});
	vector<string> reads({"TGGTACACCTTGTGTACATGGCGTTTA"});
	vector<string> newReads({""});
	vector<vector<uint>> SSKmersPerRead({{1, 2, 3}});
	string consensus(getConsensusStringBetweenSSN(0, 3, backbone, vecCons, k, reads[0]));
	correctQuery(vecCons, reads, newReads, SSKmersPerRead, 0, k, backbone);
	if(newReads[0] == "AGGTACACCTTGAGTACATGGCGTTTA"){
		passed = true;
	}
	return passed;
}


bool test_correctQuery5(){
	bool passed(false);
	vector<string> backbone({"CCTT", "ACAT","TTTA"});
	uint k(4);
	vector<string> vecCons({"AGGTACACCTT", "CCTTGAGTACAT", "", ""});
	vector<string> reads({"TGGTACACCTTGTGTACATGGCGTTTA"});
	vector<string> newReads({""});
	vector<vector<uint>> SSKmersPerRead({{1, 2}});
	string consensus(getConsensusStringBetweenSSN(0, 3, backbone, vecCons, k, reads[0]));
	correctQuery(vecCons, reads, newReads, SSKmersPerRead, 0, k, backbone);
	if(newReads[0] == "AGGTACACCTTGAGTACATGGCGTTTA"){
		passed = true;
	}
	return passed;
}

bool test_GraphConsensus(){
	bool passed(true);
	vector<string> vecReads;
	vector<vector<uint>> SSKmersPerRead;
	
	uint k(3);
	                //  ACTAGGATTC
	vecReads = {"ACTAGGATAC", "ACAAGGTTC"}; // *ACT - CTA - TAG - AGG - GGA - GAT - ATA - TAC // ACA - CAA - AAG - (AGG) - GGT - GTT - *TTC
	uint solidityT(0);
	SSKmersPerRead.push_back({0});
	SSKmersPerRead.push_back({1});
	vector<string> backbone = {"ACT", "TTC"};
	Graph graph(vecReads, SSKmersPerRead, backbone, k);
	graph.updateNodes();
	vector<vector<Node>> walkNodes(backbone.size() + 1);
	vector<string> consensusV(graph.produceConsensus( vecReads[0], solidityT, walkNodes));
	string consensus(graph.getConsensusString(0, consensusV.size()-1, consensusV,  vecReads[0]));
	passed *= (consensus == "ACTAGGATAC");  // in this example the consensus would be ACTA so we stop too early and give back the query read
	return passed;
	
}



bool test_GraphConsensus2(){  // backbone starts after 0 and finishes before end
	bool passed(true);
	vector<string> vecReads;
	vector<vector<uint>> SSKmersPerRead;
	uint k(4);
	                // true  ACTAGGATTCAGT
	vecReads = {"ACTAGGATTCATT","CTATGATTCAGT"};
	uint solidityT(1);
	SSKmersPerRead.push_back({0});
	SSKmersPerRead.push_back({1});
	vector<string> backbone = {"TAGG", "TCAG"};
	Graph graph(vecReads, SSKmersPerRead, backbone, k);
	graph.updateNodes();
	vector<vector<Node>> walkNodes(backbone.size() + 1);
	vector<string> consensusV(graph.produceConsensus( vecReads[0], solidityT, walkNodes));
	string consensus(graph.getConsensusString(0, consensusV.size()-1,  consensusV, vecReads[0]));
	passed *= (consensus == "ACTAGGATTCAGT");  
	return passed;
	
}


bool test_FullWithoutBackboneNoerror(){
	bool passed(false);
	vector<string> vecReads = {
	"TATTCTTTTCTAGATATATTTGTACACTGGTCAACATTTGCGGACTCTGTTATTATTCGATACCTAAGCTCTACGACACTCAATATCCAGCGGTAGTTTACCATTTGCCAAGCAGGTCCTGGAATGGCTTGACAGGACCCAAAGTGT",
	"TATTCTTTTCTAGATATATTTGTACACTGGTCAACATTTGCGGACTCTGTTATTATTCGATACCTAAGCTCTACGACACTCAATATCCAGCGGTAGTTTACCATTTGCCAAGCAGGTCCTGGAATGGCTTGACAGGACCCAAAGTGT",
	"TATTCTTTTCTAGATATATTTGTACACTGGTCAACATTTGCGGACTCTGTTATTATTCGATACCTAAGCTCTACGACACTCAATATCCAGCGGTAGTTTACCATTTGCCAAGCAGGTCCTGGAATGGCTTGACAGGACCCAAAGTGT",
	"TATTCTTTTCTAGATATATTTGTACACTGGTCAACATTTGCGGACTCTGTTATTATTCGATACCTAAGCTCTACGACACTCAATATCCAGCGGTAGTTTACCATTTGCCAAGCAGGTCCTGGAATGGCTTGACAGGACCCAAAGTGT",
	"TATTCTTTTCTAGATATATTTGTACACTGGTCAACATTTGCGGACTCTGTTATTATTCGATACCTAAGCTCTACGACACTCAATATCCAGCGGTAGTTTACCATTTGCCAAGCAGGTCCTGGAATGGCTTGACAGGACCCAAAGTGT",
	"TATTCTTTTCTAGATATATTTGTACACTGGTCAACATTTGCGGACTCTGTTATTATTCGATACCTAAGCTCTACGACACTCAATATCCAGCGGTAGTTTACCATTTGCCAAGCAGGTCCTGGAATGGCTTGACAGGACCCAAAGTGT",
	"TATTCTTTTCTAGATATATTTGTACACTGGTCAACATTTGCGGACTCTGTTATTATTCGATACCTAAGCTCTACGACACTCAATATCCAGCGGTAGTTTACCATTTGCCAAGCAGGTCCTGGAATGGCTTGACAGGACCCAAAGTGT",
	"TATTCTTTTCTAGATATATTTGTACACTGGTCAACATTTGCGGACTCTGTTATTATTCGATACCTAAGCTCTACGACACTCAATATCCAGCGGTAGTTTACCATTTGCCAAGCAGGTCCTGGAATGGCTTGACAGGACCCAAAGTGT"
	
		};
	vector<vector<uint>> SSKmersPerRead;
	
	uint k(9);
	uint solidityT(2);
	SSKmersPerRead = {{0,1,2}, {0,1,2}, {0,1,2}, {0,1,2} , {0,1,2}, {0,1,2}, {0,1,2}, {0,1,2}};
	vector<string> backbone = {"TATTCTTTT", "TACCTAAGC", "CCAAAGTGT"};
	Graph graph(vecReads, SSKmersPerRead, backbone, k);
	graph.updateNodes();
	vector<vector<Node>> walkNodes(backbone.size() + 1);
	vector<string> consensusV(graph.produceConsensus(vecReads[1], solidityT, walkNodes));
	vector<string> newReads(vecReads.size());
	for (uint i(0); i < vecReads.size(); ++i){
		correctQuery(consensusV, vecReads, newReads, SSKmersPerRead, i, k, backbone);
	}
	if (newReads[0] == "TATTCTTTTCTAGATATATTTGTACACTGGTCAACATTTGCGGACTCTGTTATTATTCGATACCTAAGCTCTACGACACTCAATATCCAGCGGTAGTTTACCATTTGCCAAGCAGGTCCTGGAATGGCTTGACAGGACCCAAAGTGT"){
		passed = true;
	}
	passed *= (newReads[1] ==  "TATTCTTTTCTAGATATATTTGTACACTGGTCAACATTTGCGGACTCTGTTATTATTCGATACCTAAGCTCTACGACACTCAATATCCAGCGGTAGTTTACCATTTGCCAAGCAGGTCCTGGAATGGCTTGACAGGACCCAAAGTGT") * (newReads[4] ==  "TATTCTTTTCTAGATATATTTGTACACTGGTCAACATTTGCGGACTCTGTTATTATTCGATACCTAAGCTCTACGACACTCAATATCCAGCGGTAGTTTACCATTTGCCAAGCAGGTCCTGGAATGGCTTGACAGGACCCAAAGTGT");
	return passed;
	
}
bool test_FullWithoutBackbone(){
	bool passed(false);
	// Ref TAGAACCTTTGGAGGCTAGAGTTAAGAGCTAAAGCAAGATCGGCAATAACATATTCCATTTTTTCAACGACAGTCGTAGAGTTGGGTAGGTGCGCTTCAC
	vector<string> vecReads = {"TAGAACCTTTGAGGCTAAGAGTTAACGAGCTCAAAGCAAGATCGGCAATGAACAGATATACCTATTTTTTCACGACAGTCGTAGATTTGGTAGTGCGCTGCTAC","TTATGAGACCTTTGTGAGGCTAGATGTTAAGAGCACGAAGCAAGATCGGCGAATAACTATATTCCATTTTATTCAACGACATTCGTAAGTTGGTTAGGTTGCGCTTCAC", "TAGAACTTTGGAGGAGAGTCAAGAGCTAAAGAAGATTCGGCAATAACATATTCCATTTTTTCAACGACAGTCGTAGATTGGGTAGGTGCGCTTCAC","TAGAGAACCTTGAAAGAGGCAGAGTTCAGAGCTAAAGCAGAGATCGGCAAATAACACTACTCGCATTTTTTGCAACGACAAGTCTGAGTTGAGTAGGTGCGCTTCACG","TAGAAGCTTTGGGGCTAGAGTTTAAGCACTTAGCAGATCGGCAATAAATTCCTATTTTTTCACGACATCGTAGAGTGGGGGTAGGTGCGCTTCA","TAGTAACCTTTGGCCAAGGCTAGCAGTTAAGAGCTAAAGCTAAGGGATCGGCAATAACTTAGTTCCATTTTTTTCAACGACGAGTCGTGACGGTTGGGTAGGTGCGCTTCTAC","TAGAACCTTTGGCAGGCCTAGTAGTTAAGAGCTAAGCAAGACCGCAATAACATATTCCATTTTTTCAACAGACAGTCAGTAGAGTTGGGGGATAGGTGCGTCTTCACTA","TAGAACTATTGGAGGCTAGAGTTAAGAGCTATAAGCAGGTCGGTCAATACATATTCTCATTTTTTGCAGACGACAGTCGTAGAGTTGGAGTAGGTGCGCTCAC","TAGACCTTTGGAGGGCATAGAGTTAAGAGCTAAACCAAGATCGGCAATTAACATATTCCATTTTTCAACGACAGTCGTGAGTTGGGTAGGTGCGCTTCAC","TAGTAACCTTTGGAGGCTATAGAGTAAGGAGCTAAATAAGCAAGATCGGCAATAGGTAACCACTATTCCATTTTTTCAACGACAGTCTAGAGGGTTGAGGATAGGTTGCCCTGCTCAC"

	
		};
	vector<vector<uint>> SSKmersPerRead;
	
	uint k(9);
	uint solidityT(2);
	SSKmersPerRead = {{0}, {2}, {1,2,3}, {3} , {1,3}, {1,3}, {0,2}, {3}, {2,3}, {1,2}};
	vector<string> backbone = {"TAGAACCTT", "TCGGCAATA", "TATTCCATT", "TAGGTGCGC", };
	Graph graph(vecReads, SSKmersPerRead, backbone, k);
	graph.updateNodes();
	vector<vector<Node>> walkNodes(backbone.size() + 1);
	vector<string> consensusV(graph.produceConsensus(vecReads[1], solidityT, walkNodes));
	vector<string> newReads(vecReads.size());
	for (uint i(0); i < vecReads.size(); ++i){
		correctQuery(consensusV, vecReads, newReads, SSKmersPerRead, i, k, backbone);
	}
	if (newReads[0] == "TAGAACCTTTGAGGCTAAGAGTTAACGAGCTCAAAGCAAGATCGGCAATGAACAGATATACCTATTTTTTCACGACAGTCGTAGATTTGGTAGTGCGCTGCTAC"){
		passed = true;
	}
	return passed;
	
}



bool test_FullWithBackbone(ifstream& file, ifstream& readFile){  // here we test that acquiring the reads and backbone by a file works
	bool passed(true);
	vector<string> vecReads;
	vector<string> backbone; // = {"TATTCTTTT", "CCAAAGTGT"};
	vector<vector<uint>> SSKmersPerRead;  // = {{0,1}, {1}, {0,1}, {0} , {0}, {0,1}, {1}};
	vector<vector<uint>> SSKmersPosiPerRead; 
	bool continu(readBackboneFile(file, backbone, SSKmersPerRead, SSKmersPosiPerRead));
	if (continu){
		readDataset(readFile, vecReads);
		uint k(11);
		uint solidityT(2);
		Graph graph(vecReads, SSKmersPerRead, backbone, k);
		graph.updateNodes();
		vector<vector<Node>> walkNodes(backbone.size() + 1);
		vector<string> consensusV(graph.produceConsensus(vecReads[0], solidityT, walkNodes));
		string consensus(graph.getConsensusString(0, consensusV.size()-1,  consensusV, vecReads[0]));
		passed *= (consensus == "TATTCTTTTCTAGATATATTTGTACACTGGTCAACATTTGCGGACTCTGTTTTTATTCGATACCTTAGCTCTACGACACTCAATATCCAGCGGTAGTTTACCATTTGCCAGCAGGTCCTGGAATGGCTTGACAGGACCCAAAGTGT");
		return passed;
	} else {
		cout << "something went wrong with the backbone file, maybe empty ?" << endl;
		return 0;
	}

}


//~ int main(int argc, char ** argv){
	//~ string readFileName(argv[1]);
	//~ string fileName(argv[2]);
	//~ uint k(uint(stoi(argv[3])));
	//~ uint solidity(uint(stoi(argv[4])));
	//~ ifstream file(fileName);
	//~ ifstream readFile(readFileName);


	//~ bool passed;
	//~ passed = test_getWindowsInQuery();
	//~ if (passed){
		//~ cout << "test windows... passed" << endl;
	//~ } else {
		//~ cout << "test windows... not passed" << endl;
	//~ }

	//~ passed = test_getWindowsInOthers();
	//~ if (passed){
		//~ cout << "test windows... passed" << endl;
	//~ } else {
		//~ cout << "test windows... not passed" << endl;
	//~ }

	//~ passed = test_chopReadsInWindows();
	//~ if (passed){
		//~ cout << "test windows... passed" << endl;
	//~ } else {
		//~ cout << "test windows... not passed" << endl;
	//~ }


	//~ passed = test_getWindowsOnReads();
	//~ if (passed){
		//~ cout << "test windows... passed" << endl;
	//~ } else {
		//~ cout << "test windows... not passed" << endl;
	//~ }

	//~ processReads(file, readFile, k, solidity);

	//~ passed = test_NodeConstrCopy();
	//~ if (passed){
		//~ cout << "test Node constr... passed" << endl;
	//~ } else {
		//~ cout << "test Node constr... not passed" << endl;
	//~ }
	//~ passed = test_NodeConstr();
	//~ if (passed){
		//~ cout << "test Node constr... passed" << endl;
	//~ } else {
		//~ cout << "test Node constr... not passed" << endl;
	//~ }
	//~ passed = test_NodeConstrSSN();
	//~ if (passed){
		//~ cout << "test Node constr... passed" << endl;
	//~ } else {
		//~ cout << "test Node constr... not passed" << endl;
	//~ }
	
	//~ passed = test_updateRegular();
	//~ if (passed){
		//~ cout << "test Node update... passed" << endl;
	//~ } else {
		//~ cout << "test Node update... not passed" << endl;
	//~ }


	//~ passed = test_updateRegularToRepeat();
	//~ if (passed){
		//~ cout << "test Node update... passed" << endl;
	//~ } else {
		//~ cout << "test Node update... not passed" << endl;
	//~ }


	//~ passed = test_updateRepeat();
	//~ if (passed){
		//~ cout << "test Node update... passed" << endl;
	//~ } else {
		//~ cout << "test Node update... not passed" << endl;
	//~ }

	//~ passed = test_findInNeighbour();
	//~ if (passed){
		//~ cout << "test Node find neighbour... passed" << endl;
	//~ } else {
		//~ cout << "test Node find neighbour... not passed" << endl;
	//~ }


	//~ passed =test_findOutNeighbour();
	//~ if (passed){
		//~ cout << "test Node find neighbour... passed" << endl;
	//~ } else {
		//~ cout << "test Node find neighbour... not passed" << endl;
	//~ }


	//~ passed = test_findOutKNeighbour();
	//~ if (passed){
		//~ cout << "test Node find k-2 neighbour... passed" << endl;
	//~ } else {
		//~ cout << "test Node find k-2 neighbour... not passed" << endl;
	//~ }

	
	//~ passed = test_findOutKnNeighbour();
	//~ if (passed){
		//~ cout << "test Node find k-n neighbour... passed" << endl;
	//~ } else {
		//~ cout << "test Node find k-n neighbour... not passed" << endl;
	//~ }
	
	//~ passed = test_changeSSNRegular();
	//~ if (passed){
		//~ cout << "test Node SSN change... passed" << endl;
	//~ } else {
		//~ cout << "test Node SSN change... not passed" << endl;
	//~ }

	//~ passed = test_GraphConstr();
	//~ if (passed){
		//~ cout << "test Graph constr... passed" << endl;
	//~ } else {
		//~ cout << "test Graph constr... not passed" << endl;
	//~ }

	//~ passed = test_GraphUpdate();
	//~ if (passed){
		//~ cout << "test Graph update... passed" << endl;
	//~ } else {
		//~ cout << "test Graph update... not passed" << endl;
	//~ }


	//~ passed = test_compactBackbone();
	//~ if (passed){
		//~ cout << "test compaction... passed" << endl;
	//~ } else {
		//~ cout << "test compaction... not passed" << endl;
	//~ }


	//~ passed = test_GraphConsensus();
	//~ if (passed){
		//~ cout << "test Graph consensus... passed" << endl;
	//~ } else {
		//~ cout << "test Graph consensus... not passed" << endl;
	//~ }

	// todo re-write one

	//~ passed = test_GraphConsensus2();
	//~ if (passed){
		//~ cout << "test Graph consensus... passed" << endl;
	//~ } else {
		//~ cout << "test Graph consensus... not passed" << endl;
	//~ }

	//~ passed = test_getConsensusString();
	//~ if (passed){
		//~ cout << "test to string consensus... passed" << endl;
	//~ } else {
		//~ cout << "test to string consensus... not passed" << endl;
	//~ }
	
	//~ passed = test_getConsensusString2();
	//~ if (passed){
		//~ cout << "test to string consensus... passed" << endl;
	//~ } else {
		//~ cout << "test to string consensus... not passed" << endl;
	//~ }

	//~ passed = test_getConsensusString3();
	//~ if (passed){
		//~ cout << "test to string consensus... passed" << endl;
	//~ } else {
		//~ cout << "test to string consensus... not passed" << endl;
	//~ }

	
	//~ passed = test_correctQuery();
	//~ if (passed){
		//~ cout << "test correction... passed" << endl;
	//~ } else {
		//~ cout << "test correction... not passed" << endl;
	//~ }
	
	//~ passed = test_correctQuery2();
	//~ if (passed){
		//~ cout << "test correction... passed" << endl;
	//~ } else {
		//~ cout << "test correction... not passed" << endl;
	//~ }


	
	//~ passed = test_correctQuery3();
	//~ if (passed){
		//~ cout << "test correction... passed" << endl;
	//~ } else {
		//~ cout << "test correction... not passed" << endl;
	//~ }
	
	//~ passed = test_correctQuery4();
	//~ if (passed){
		//~ cout << "test correction... passed" << endl;
	//~ } else {
		//~ cout << "test correction... not passed" << endl;
	//~ }

	//~ passed = test_correctQuery5();
	//~ if (passed){
		//~ cout << "test correction... passed" << endl;
	//~ } else {
		//~ cout << "test correction... not passed" << endl;
	//~ }

	//~ passed = test_FullWithoutBackboneNoerror();
	//~ if (passed){
		//~ cout << "test Full no error... passed" << endl;
	//~ } else {
		//~ cout << "test Full noerror ... not passed" << endl;
	//~ }
	
	//~ passed = test_FullWithoutBackbone();
	//~ if (passed){
		//~ cout << "test Full error... passed" << endl;
	//~ } else {
		//~ cout << "test Full error ... not passed" << endl;
	//~ }

	
	//~ passed = test_FullWithBackbone(file, readFile);
	//~ if (passed){
		//~ cout << "test Full... passed" << endl;
	//~ } else {
		//~ cout << "test Full... not passed" << endl;
	//~ }




	//~ return 0;
//~ }

