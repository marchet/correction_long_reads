#include <vector>
#include <string>
#include <iostream>
#include <unordered_map>
#include <unordered_set>


#include "backbone.hpp"
#include "processReads.hpp"
#include "utils.hpp"

#ifndef CORRECGPH
#define CORRECGPH




using namespace std;

typedef unsigned int uint;




// ----- Class Node ----- //

class Node {
 public:
	bool isSSNode;
	bool isRepeat;
	uint index;
	string kmer;
	vector<pair<int, int>> SSNodes;
	uint solidity;
	uint seen;
	uint overlapToNext;
	// constructors
	Node();
	Node(const Node& node);
	Node(uint indexI, const string& kmerI, int indexThisSSN);  // SSN
	Node(uint indexI, const string& kmerI, uint seen, int indexPrevSSN, int indexNextSSN, bool repeated);   // regular and repeat
	void update(int indexPrevSSN, int indexNextSSN, bool repeat);
	void updateRepeat();

	void changeSSN(unordered_map<string, uint>& listNodes, uint k, vector<Node>& vecN);
	void changeSSNOut(unordered_map<string, uint>& listNodes, uint k, vector<Node>& vecN);

	vector<Node> findOutNeighbour(const unordered_map<string, uint>& listNodes, vector<Node>& vecN);
	vector<Node> findInNeighbour(const unordered_map<string, uint>& listNodes, uint k, vector<Node>& vecN);
	vector<Node> findOutKNeighbour(const unordered_map<string, uint>& listNodes, uint overlap, vector<Node>& vecN);

};

// ----- end Class Node ----- //

// ----- Class Node ----- //

class Graph {
 public:
	uint sizeK;
	unordered_map<string, uint> listNodes2;
	vector<Node> vecNodesGraph;
	vector<string> backbone;
	Graph(const vector<string>& vecReads, const vector<vector<uint>>& SSKmersPerRead, const vector<string>& backbone, uint k);
	void updateNodes();
	bool getBeforeFirst(vector<Node>& walk, uint solidityThreshold);
	bool getAfterLast(vector<Node>& walk, uint solidityThreshold);
	bool findNextNode(vector<Node>& walk, int& nextSS, uint solidityThreshold, unordered_set<uint>& seenRepeatsBtwSSN);
	vector<string> produceConsensus( const string& queryRead, uint solidityThreshold, vector<vector<Node>>& walkNodes);
	string getConsensusString(const uint index1, const uint index2, const vector<string>& vecCons, const string& queryRead);

};

bool compareNodesBySolid(const Node& node1, const Node& node2);

void correctData(ifstream& readFile, const uint ksize, const uint solidity);

string getConsensusStringBetweenSSN(const uint index1, const uint index2, vector<string>& backbone, const vector<string>& vecCons, const uint k, const string& queryRead);

//~ bool processReads(ifstream& file, ifstream& readFile, uint k, uint solidity);
bool processReads(ifstream& readFile, uint k, uint solidity, vector<string>& backbone, vector<vector<uint>>& SSNperRead, vector<vector<uint>>& distancesSSN, vector<string>& vecReads);

//~ void processReads(ifstream& file, ifstream& readFile, uint k, uint solidity);

#endif
