
#include <vector>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <unordered_set>

#include "processReads.hpp"

// ----- IO functions ----- //
// read the input reads file and store the sequences in a string vector
void readDataset(ifstream& readFile, vector<string>& readSet){
	string sequence;
	while (not readFile.eof()){
        getline(readFile, sequence);
		getline(readFile, sequence);
		if (not sequence.empty()){
			readSet.push_back(sequence);
		}
	}
}



bool readBackboneFile(ifstream& file, vector<string>& backbone, vector<vector<uint>>& SSKmersPerRead, vector<vector<uint>>& SSKmersPosiPerRead){
	string line;
	getline(file, line);

	if (not line.empty()){
		backbone = split(line, ' ');
		while (not file.eof()){
			getline(file, line);
			vector<uint> vec2;
			if (not line.empty()){
				vector<string> vec(split(line, ' '));
				for (string v : vec){
					vec2.push_back(uint(stoi(v)));
				}
				SSKmersPerRead.push_back(vec2);
			} else { // read without backbone kmers
				SSKmersPerRead.push_back({});
			}
			getline(file, line);
			vector<uint> vec3;
			if (not line.empty()){
				vector<string> vec(split(line, ' '));
				for (string v : vec){
					vec3.push_back(uint(stoi(v)));
				}
				SSKmersPosiPerRead.push_back(vec3);
			} else { // read without backbone kmers
				SSKmersPosiPerRead.push_back({});
			}
			
		}
		return 1;
	}
	return 0;
}


void getWindowsInQuery(uint indexQuery, vector<string>& vecReads, vector<string>& backbone, vector<vector<uint>>& SSNperRead, uint k, uint w, vector<pair<uint, uint>>& listSSKWindows, vector<pair<int, int>>& listPairsSSKWindows){
	//~ vector<pair<uint, uint>> listSSKWindows;
	//~ vector<pair<uint, uint>> listPairsSSKWindows;
	uint j(0), posLastKmer;
	string kmer;
	if (SSNperRead[indexQuery].size() > 1){
		for (uint posi(0); posi < vecReads[indexQuery].size() - k + 1; ++posi){
			kmer = vecReads[indexQuery].substr(posi, k);
			if (j < SSNperRead[indexQuery].size()){
				if (kmer == backbone[SSNperRead[indexQuery][j]]){
					//~ cout << "POSI "  << posi << endl;
					if (listSSKWindows.empty()){  // first kmer
						posLastKmer = posi;
						listSSKWindows.push_back({SSNperRead[indexQuery][j], posi});  // index of the SSK in the backbone and position in the query read
					} else {
						if (posi >= posLastKmer + w - w/5){
							posLastKmer = posi;
							listSSKWindows.push_back({SSNperRead[indexQuery][j], posi});  // index of the SSK in the backbone and position in the query read
						}
					}
					++j;
				}
			} else {
				break; // reached last bb kmer of the read
			}
		}
	}
	if (listSSKWindows.size() > 1){
		for (uint i(0); i < listSSKWindows.size() - 1; ++i){
			listPairsSSKWindows.push_back({listSSKWindows[i].first, listSSKWindows[i+1].first});
		}
	}
}





// only use if listSSKWindowsQuery has enough windows
void getWindowsInEachRead(vector<string>& vecReads, vector<string>& backbone, vector<vector<uint>>& SSNperRead, vector<pair<int, int>>& listPairsSSKWindows, vector<vector<pair<int, int>>>& listWindows){
	for (uint read(0); read < vecReads.size(); ++read){
		vector<pair<int, int>> listSSKWindowsOthers(listPairsSSKWindows.size());
		//~ if (read != indexQuery){
			uint bb(0);
			listSSKWindowsOthers[0] = {-1,-1};
			for (uint pair(0); pair < listPairsSSKWindows.size(); ++pair){  // for a pair 
				//~ listSSKWindowsOthers[pair] = {-1,-1};
				while (bb < SSNperRead[read].size()){
					if (SSNperRead[read][bb] <= uint(listPairsSSKWindows[pair].first)){
						listSSKWindowsOthers[pair].first = SSNperRead[read][bb];
					}
					if (SSNperRead[read][bb] >= uint(listPairsSSKWindows[pair].second)){
						if (listSSKWindowsOthers.back().first != -1){
							listSSKWindowsOthers[pair].second = SSNperRead[read][bb];
							if (pair < listSSKWindowsOthers.size() - 1){
								listSSKWindowsOthers[pair+1].first = listSSKWindowsOthers[pair].second;
							}
						}
						break;
					}
					++bb;
				}
			}
		//~ } else {
			//~ listSSKWindowsOthers = listPairsSSKWindows;
		//~ }
		listWindows.push_back(listSSKWindowsOthers);
	}
	for (uint i(0); i < listWindows.size(); ++i){
		//~ if (i != indexQuery){
			for (uint j(1); j <  listWindows[i].size() - 1; ++j){
				if (listWindows[i][j].first == -1){
					if (listWindows[i][j-1].first != -1){
						listWindows[i][j].first = listWindows[i][j-1].first;
					}
				}
				if (listWindows[i][j].second == -1){
					if (listWindows[i][j+1].second != -1){
						listWindows[i][j].first = listWindows[i][j+1].second;
					}
				}
			}
		//~ }
	}
	for (int i(0); i < listPairsSSKWindows.size(); ++i){
		cout << listPairsSSKWindows[i].first << " " <<  listPairsSSKWindows[i].second  << ",";
	}
	cout << endl << "*************" << endl;
	for (uint i(0); i < listWindows.size(); ++i){
		for (uint j(1); j <  listWindows[i].size() - 1; ++j){
			cout << listWindows[i][j].first << " " << listWindows[i][j].second << ",";
		}
		cout << endl;
		cout << endl;
	}
}


//~ void getWindowsInReads(uint indexQuery, vector<string>& vecReads, vector<string>& backbone, vector<vector<uint>>& SSNperRead, vector<pair<uint, uint>>& windowDelim, vector<pair<int, int>>& listPairsSSKWindows, vector<vector<pair<int, int>>>& listWindows){
	//~ for (uint read(0); read < vecReads.size(); ++read){
		//~ vector<pair<int, int>> listSSKWindowsReads(listPairsSSKWindows.size());
		//~ uint bb(0);
		//~ listSSKWindowsOthers[pair] = {-1,-1};
		//~ for (uint pair(0); pair < listPairsSSKWindows.size(); ++pair){  // for a pair of the query
			//~ while (bb < SSNperRead[read].size()){
				//~ if (SSNperRead[read][bb] <= (uint)listPairsSSKWindows[pair].first){
					//~ listSSKWindowsOthers[pair].first = SSNperRead[read][bb];
				//~ }
				//~ if (SSNperRead[read][bb] >= (uint)listPairsSSKWindows[pair].second){
					//~ if (listSSKWindowsOthers.back().first != -1){
						//~ listSSKWindowsOthers[pair].second = SSNperRead[read][bb];
					//~ }
					//~ break;
				//~ }
				//~ ++bb;
			//~ }
		//~ }
	//~ }
//~ }





// will chop between a pair 
void chopReadsInWindows(vector<pair<uint, uint>>& listSSKWindows, uint k, vector<string>& subseqVec, vector<vector<pair<int, int>>>& listWindows, uint pair, uint indexQuery, vector<string>& vecReads, vector<string>& backbone, vector<vector<uint>>& SSNperRead){
	// in query read
	uint firstKmerPosi(listSSKWindows[pair].second);
	uint lastKmerPosi(listSSKWindows[pair + 1].second);
	string subseq(vecReads[indexQuery].substr(firstKmerPosi, lastKmerPosi - firstKmerPosi + k)), kmer;
	subseqVec.push_back(subseq);
	// in other reads
	for (uint read(0); read < vecReads.size(); ++read){
		if (read != indexQuery){
			int firstKmer(listWindows[read][pair].first);
			int lastKmer(listWindows[read][pair].second);
			if (firstKmer != -1 and lastKmer != - 1){
				string kmer1(backbone[firstKmer]);
				string kmer2(backbone[lastKmer]);
				uint posi1, len;
				bool found(false);
				for (uint posi(0); posi < vecReads[read].size() - k + 1; ++posi){
					kmer = vecReads[read].substr(posi, k);
					if (kmer == kmer1){
						posi1=posi;
						found = true;
						len = 0;
					} else {
						++len;
						if (found){
							if (kmer == kmer2){
								len += k;
								subseq = vecReads[read].substr(posi1, len);
								subseqVec.push_back(subseq);
								break;
							}
						}
					}
				}
			}
		}
	}
}



void getWindowsOnReads(vector<string>& vecReads, vector<string>& backbone, vector<uint>& distancesSSN, uint k, uint w, vector<pair<int, int>>& listPairsSSKWindows){
	uint dist(0), d(1);
	vector <pair<uint, uint>> windowDelim;
	windowDelim.push_back({0,0});
	while (d < distancesSSN.size()){
		if (dist+distancesSSN[d] < w){
			if (dist+distancesSSN[d+1] > 0.2 * w){  // in case we  will have a long windows we try to get a little smaller one
				if (dist+distancesSSN[d] >= int(w - w * 0.2)){
					//~ cin.get();
					windowDelim.push_back({d, dist});
					dist = 0;
				} else {
					dist += distancesSSN[d];
					//~ ++d;
				}
			} else {
				dist += distancesSSN[d];
			}
		} else {
			if (dist == 0){
				dist = distancesSSN[d];
			}
			windowDelim.push_back({d, dist+distancesSSN[d]});
			dist = 0;
		}
		//~ cout << "dist " << dist << endl;
		++d;
	}
	dist = 0;
	if (windowDelim.back().first != distancesSSN.size() - 1){
		uint dd(windowDelim.back().first);
		uint dist2(windowDelim.back().second);
		while (dd < distancesSSN.size()){
			if (dd - 1 == distancesSSN.size() - 2){
				if (dist+distancesSSN[dd-1] >= (int)(w - w * 0.5)){  // add a smaller last window
					windowDelim.push_back({dd, dist+distancesSSN[d]});
				} else {
					windowDelim.back() = {dd, dist2};  // longer last window to include until the last element
				}
				break;
			} else {
				dist += distancesSSN[dd];
				dist2 += distancesSSN[dd];
			}
			++dd;
		}
	}
	//~ for (auto&& dd: windowDelim){
		//~ cout << "* " << dd.first << " " << dd.second <<  endl;
 	//~ }
 	for (uint i(0); i < windowDelim.size() - 1; ++i){
		listPairsSSKWindows.push_back({(int)windowDelim[i].first, (int)windowDelim[i+1].first});
	}
 	//~ vector<pair<int, int>>& 
	
}


//~ bool getReadsAndBackbone(ifstream& file, vector<string>& readSet, uint k, uint solid, vector<string>& backbone, vector<vector<uint>>& SSKmersPerRead, vector<vector<uint>>& SSKmersPosiPerRead){
	//~ readDataset(file, readSet);
	//~ THEFUNCTION(readSet, k, solid, backbone, SSKmersPerRead, SSKmersPosiPerRead);
//~ }





//~ int THEFUNCTION(const vector<string>& readsVector, const uint k, const uint solid, vector<string>& kmersBackbonevector, vector<vector<uint>>& backbones,vector<vector<uint>>& backbonesPositions){


