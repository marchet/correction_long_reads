#include <fstream>
#include <cstring>
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <unordered_set>
#include <unordered_map>

#include "utils.hpp"

#ifndef BB
#define BB

using namespace std;





vector<string> withoutAdj(unordered_map<string,vector<string>>& prev, unordered_map<string,vector<string>>& next, unordered_map<string,uint>& kmerCount, vector<string>& candidate);


void eraseKmer(unordered_map<string,vector<string>>& prev, unordered_map<string,vector<string>>& next, unordered_map<string,uint>& kmerCount,string kmer);



unordered_set<string> getFirstKmers(uint depth, unordered_map<string,uint>& kmerCount, vector<vector<string>>& sketchVector);


unordered_set<string> getLastKmers(uint depth, unordered_map<string,uint>& kmerCount, vector<vector<string>>& sketchVector);

int THEFUNCTION(const vector<string>& readsVector, const uint k, const uint solid, vector<string>& kmersBackbonevector, vector<vector<uint>>& backbones, vector<uint>& distances);



#endif

