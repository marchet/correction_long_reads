#include <vector>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <unordered_set>

#include "consensus.hpp"
#include "processReads.hpp"
#include "utils.hpp"

#ifndef TEST
#define TEST

using namespace std;

typedef unsigned int uint;



// test read processing

bool test_getWindowsInQuery();


bool test_getWindowsInOthers();


bool test_chopReadsInWindows();

bool test_getWindowsOnReads();





// tests on Node objects


bool test_NodeConstrCopy();


bool test_NodeConstr();


bool test_NodeConstrSSN();


bool test_updateRegular();

bool test_updateRegularToRepeat();



bool test_updateRepeat();

bool test_changeSSNRegular();


bool test_findOutKNeighbour();



bool test_findOutKnNeighbour();


bool test_findOutNeighbour();

bool test_findInNeighbour();



// test on object Graph


bool test_GraphConstr();


// todo do a real update
bool test_GraphUpdate();



bool test_getConsensusString();

bool test_getConsensusString2();


bool test_getConsensusString3();

bool test_compactBackbone();


bool test_correctQuery();



bool test_correctQuery2();


bool test_correctQuery3();




bool test_correctQuery4();


bool test_correctQuery5();
bool test_GraphConsensus();



bool test_GraphConsensus2();


bool test_FullWithoutBackboneNoerror();
bool test_FullWithoutBackbone();


bool test_FullWithBackbone(ifstream& file, ifstream& readFile);

#endif
