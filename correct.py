#!/usr/bin/env python



sizeKmer = 9


def makeConsensus(PKM, LP, sizeSmallKmer): #PKM list of list of kmers   #LP starting kmer, position of the starting kmer, list associated

    max_cs = LP[0][0] #consensus seq
    max_ps = LP[0][1] #position
    max_sc = LP[0][2] #list
    kk = 0.0
    kk = kk + max_ps
    k = int(kk)
    while len(PKM[k])!=0 and len(LP) != 0:
        NLP = []
        for p in range(len(LP)):
            cs = LP[p][0] # consensus seq
            sc = LP[p][1] # score 

            L = []
            #~ i = max (0,len(cs)-50)
            i = max (0,len(cs)-sizeSmallKmer-50)
            for j in range(i,len(cs)-sizeSmallKmer+1):
                L.append(cs[j:j+sizeSmallKmer])
            
            for kmer in PKM[k]:
                if kmer not in L and cs[-sizeSmallKmer+1:] == kmer[:-1]:
                    new_sc = sc+PKM[k][kmer]
                    new_cs = cs+kmer[sizeSmallKmer-1]
                    NLP.append([new_cs,new_sc])
                    if new_sc > max_sc:
                        max_sc = new_sc
                        max_cs = new_cs
                        max_ps = k

        LP = sorted(NLP,reverse=True,key=itemgetter(1))
        if len(LP) > 100:
            LP = LP[:100]

        kk = kk + 1.1
        k = int(kk)
    return max_sc, max_cs, max_ps


def main(args):
    # read reference
    ref = sys.argv[1]
    sizeSmallKmer = int(sys.argv[3])
    #~ ff = open("Lambda/reference.fasta")
    ff = open(ref)
    com = ff.readline()
    seq = ff.readline()
    refLambda = ""
    while seq != "":
        refLambda = refLambda+seq[:-1]
        seq = ff.readline()
    ff.close()
    print "size reference: ",len(refLambda)

    # read Lambda Long Read

    LR = []
    nbnt = 0
    reads= sys.argv[2]
    ff = open(reads)
    com = ff.readline()
    while com !="":
        seq = ff.readline()[:-1]
        LR.append(seq)
        nbnt = nbnt + len(seq)
        com = ff.readline()
    ff.close()

    print "number of reads:", len(LR)
    print "coverage:", nbnt/len(refLambda)

    # compare all reads with all reads just by counting kmers
    # 2 reads with the same ordered list of kmers may overlap

    ff = open("result.fasta","w")
    nbctg = 0
    for i in range(len(LR)):
        seqi = LR[i]
        nbseq = 0
        print
        print "LENGTH:",len(seqi)

        # DKM contains kmers of sequence i
        DKM = {}
        for k in range(len(seqi)-sizeKmer+1):
            kmer = seqi[k:k+sizeKmer]
            DKM[kmer] = k

        # LK is a list of kmer dictionaries at position k
        LK = []
        for k in range(len(seqi)-sizeSmallKmer+1):
            kmer = seqi[k:k+sizeSmallKmer]
            LK.append({})
            LK[k][kmer] = 1
        for k in range(50):
        #~ for k in range(2*sizeSmallKmer):
            LK.append({})

        for j in range(len(LR)):
            if i != j:
                seqj = LR[j]
                # L is the list of commun kmers between seq i and seq j
                L = []
                for k in range(len(seqj)-sizeKmer+1):
                    kmer = seqj[k:k+sizeKmer]
                    if kmer in DKM:
                        L.append([DKM[kmer],k,k-DKM[kmer]])
                if len(L) > len(seqi)/10 or len(L) > 40:
                    # clustering of L based on the indices of kmers
                    CL = []
                    for t in L:
                        ok = False
                        for k in range(len(CL)):
                            x = CL[k][0]
                            if abs(t[2]-x[2]) < len(seqi)/20:
                                ok = True
                                CL[k].append(t)
                        if ok == False:
                            CL.append([t])
                    L = []
                    for l in CL:
                        if len(l) > 5:
                            L.append(l)
                    # we keep only pair of sequences with unique match
                    if len(L) == 1:
                        LL = L[0]
                        starti = LL[0][0]
                        endi   = LL[len(LL)-1][0]
                        startj = LL[0][1]
                        endj   = LL[len(LL)-1][1]
                        ok = False
                        # test the following configuration:
                        # seqi           <---------------------------->
                        #                | | | | | | | | |
                        # seqj   <------------------------>
                        if starti < 50 and abs((len(seqj)-startj) - endi) < 50:
                            ok = True
                        # seqi    <---------------------------->
                        #                    | | | | | | | | |
                        # seqj              <------------------------>
                        elif startj < 50 and abs((len(seqi)-starti) -endj) < 50:
                            ok = True
                        # seqi      <---------------------------->
                        #                | | | | | | | | |
                        # seqj           <---------------->
                        elif startj < 50 and len(seqj) < len(seqi) and abs(len(seqj)-endj) < 50:
                            ok = True
                        # seqi       <--------------->
                        #             | | | | | | | | 
                        # seqj   <------------------------>
                        elif starti < 50 and len(seqi) < len(seqj) and abs(len(seqi)-endi) < 50:
                            ok = True
                        else:
                            ok = False

                        if ok == True:
                            nbseq = nbseq+1
                            for k in range(len(LL)-1):
                                starti = LL[k][0]
                                endi   = LL[k+1][0]
                                startj = LL[k][1]
                                d = 0
                                for ki in range(starti,min(endi,len(seqi)-sizeSmallKmer+1)):
                                    for kj in range(max(0,startj+d-30),min(startj+d+30,len(seqj)-sizeSmallKmer)):
                                        kmer = seqj[kj:kj+sizeSmallKmer]
                                        if kmer not in LK[ki]:
                                            LK[ki][kmer] = 0
                                        LK[ki][kmer] = LK[ki][kmer]+1
                                    d=d+1      
        # correction step        
        #~ if nbseq >= 15:
        if nbseq >= 10:
            # keep only solid kmers
            for k in range(len(LK)):
                l = []
                for kmer in LK[k]:
                    if LK[k][kmer] < 3 :
                        l.append(kmer)
                for kmer in l:
                    del LK[k][kmer]

            cpos = 0
            LS = []

            while cpos < len(seqi) - sizeSmallKmer + 1:
            #~ while cpos < len(seqi) - 2*sizeSmallKmer:

                # find starter kmer

                find_starter = False
                while find_starter == False and cpos < sizeSmallKmer + 1: # look for a starting kmer in a small region in the beginning
                #~ while find_starter == False and cpos < len(seqi):
                    for kmer in LK[cpos]:
                        #~ if LK[cpos][kmer] >= 8 and kmer not in LS:
                        if LK[cpos][kmer] >= 3 and kmer not in LS:
                            LS.append(kmer)
                            find_starter = True
                            break
                    if find_starter == False:
                        cpos = cpos+1

                # find the consensus sequence built from a list of overlaping kmers

                if find_starter == True:
                    score, consensus, pos = makeConsensus(LK,[[kmer,cpos,LK[cpos][kmer]]], sizeSmallKmer)
                    if pos-cpos > 100: # here we should rather check if the corrected sequence is at least a certain percentage of the length of the original seq to be corrected
                        print "start:",cpos, "  end:",pos, "  len:",len(consensus)
                        cpos = pos

                        ff.write(">ctg_"+str(nbctg)+"|"+str(len(seqi))+"|"+str(len(consensus))+"\n")
                        ff.write(consensus+"\n")
                        nbctg = nbctg+1
    ff.close()
    return 0;

if __name__ == '__main__':
    import sys
    from operator import itemgetter
    sys.exit(main(sys.argv))
